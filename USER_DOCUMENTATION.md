#### Short Summary
An augmented reality video game where the goal is to target balloon objects in order to acquire game points within a set time limit.

#### Gameplay
Players will shoot various balloon targets for points using a harry potter kano wand. Players will be awarded different point amounts for each balloon shot by the wand. In game the player will be shown their wands position as well as other GUI elements such as current score, time remaining in round, particular color balloon hits, total balloon hits, etc..

#### Mechanics
Player will aim at targets with a bluetooth wand. Player will have various types of wand spell motions that they can utilize to destroy said balloons. Further explanation is shown further down in the features section.

#### Game elements
Levels for Balloon Storm! Run! will be show in the room the player is currently in when they start the game while connecting the the player's onboard device camera. Due to the game being in augmented reality there are no set backdrops. Being able to view your scores from a menu will be available. There is also funcitonality to connect your wand to the application.

#### Assets
Current assets are limited to a single 3D rendered balloon shaped model that is shown as either a yellow, blue or red sphere. A color changing wand reticle will be dispalyed. 

## Features & Core Concepts: Description Guide (Below)

### Registration
<p>
The player will be shows a signin-registration screen in which they'll be able to enter the game by entering their desired credentials. Player will first be in the signin page but must first choose registration before beginning the game. A bypass login button is available to any users who do not wish to give credentials, may still paly the game but will not have a registered account. After registering their account, said credentials will be needed for future uses of the applicaiton. credentials are stored using mongoDB Realm and can only be accessed by authorized personnel for user data protection.
</p>

### Player Name
<p>
The player will enter their desired user name. After entering their username, they may slide out the side menu from the left side fo the screen and will see their name displayed at the top. 
</p>

### Change Player Name
<p>
Once the player has accessed the slideout menu, they may select the "Change Name" button. They may then re-enter their username and their new name will be displayed at the top of the slideout menu from there on out.
</p>

### Pairing Your Wand
<p>
A player may connect their Kano Harry Potter wand when desired. After inserting batteries into the backside of the acceleramoter inside the bottom plastic compartment, users can select the "Pair Your Wand" button. A drop down menu will be displayed. The player will then hold down the singular button, wait for the Kano wand name to appear in the selection menu, and then may select to connect said wand. The player may choose to "Test BLE" where they may test out their wand before palying their game.
</p>

### Start The Game / Timer
<p>
After pairing your wand, the game may be started by pressing the "Play" button on the slideout menu. This button displays the gameplay space where a yellow wand reticle will display. After a few short, allotted seconds, the user will then notice that the wand "pyramid" or "triangle" shaped reticle is moving with their wand movements. User can hold down the singular wand button to "zero" out the wand thereby resetting the reticle to its center position. When the "Play" button is pressed, the game timer will begin.
</p>

### Balloons, Balloon Popping, Displayed Points & Timer
<p>
Once the reticle is connected to the Kano wand, the player will see a singular balloon. Depending on what color it is, the reticle must be the same color as said balloon and dragged into the balloon's hitbox where it will pop and dissapear and then reappear after a short, randomized lag-time. This despawn/respawn action triggers an increase of the player's total points. Points are displayed in the top right corner of the play-space. Stats displayed tell the player how many points, hits, and color-specific balloon hits they've acquired. A timer displayed in the very top-right corner gives the player an idea of how much time they have left in the playthrough. To pop a balloon, the reticle must be turned to the correct color as the spawned balloon.
</p>

### Wand Spells & Point System
<p>
There are 3 different spells that the user can cast. A Lightning bolt, indicated by a yellow reticle, an Ocean Wave indicated by a blue reticle, and a Fire Blast indicated by a red reticle. The lightning spell to pop a yellow balloon for 100 pointscan be cast by simply keeping yourhand steady and the tip of the wand in the same position, careful to not take it off course. An Ocean Wave spell can be done to pop a blue balloon for 500 points which can be cast by pressing and holding the singular wand button and then moving your want tip upwards like a "wave" and then letting go of your wand button, thereby turning the reticle blue. A Fireball spell can be cast to pop a red balloon for a stagering 1,000 points by pressing and holding down the singular button wand, then tilting your wand tip in a downward direction, like fire spreading accross the ground or lava pooling in the earth's crust, and then letting go of the wand button, thereby turning the reticle red. Once the wand reticle is the same color as the balloon object, popping of said balloon can be done, and the player can then increase their total points. Other miscellaneous stats are kept track of on the top right section of the play space and can be referred to at any time.
</p>

### End & Reset Your Game
<p>
Once the game timer has run downa dn out to zero seconds, the player may then open up their side menu and press the "Quit" button. This action, however, will refresh their game stats, and unsaved progress will be lost. To save your progress or your results, a screen-capture of the application screen is recommended to share with your friends online on how you performed. If the player wish to play the game again, and thereby resetting the game back up, they may simply select the "Play" button once again.
</p>

---

#### Installation / User Manual
Setup:

    Note: If you already have the file system on your local computer, you may skip steps 1-3.

    1.	Download an IDE of your choice. Visual Studio Code is recommended.
    2.	Generate SSH Key / Add key to Gitlab
    3.	Clone repository
    4.	Install node.js with its defaults: https://nodejs.org/en/
    5.	Open a new terminal in Visual Studio Code and run the following commands:
        a.	>> npm install -g @ionic/cli
        b.	Run Project- while in the “balloonStorm_V2” directory:
            i. >> ionic serve
            ii. If node.js is asks for permission, click “Allow Access”
            iii. Install the react-scripts whenever prompted.
            iv.	If project does not run, continue with direections.
        c.	>> npm install @capacitor-community/bluetooth-le
        d.	>> npm install realm
        e.	>> npm install --save typescript @types/node @types/react @types/react-dom @types/jest
        f.	>> npm install three @react-three/fiber
        g.	>> npm install @react-three/drei
        h.	>> npm install @react-three/xr
        i.	>> npm i styled-components @types/styled-components
        j.	>> npm install realm-web
        k.	>> npm install @apollo/client
        l.  >> npm install --save graphql ra-data-graphql
        m.  >> npm install react-countdown --save
    6.	At this point, the project should be running on your web browser.
        a.	If application is not running, please contact the support team.
    7. Android Studio (Optional for Mobile)
        a. Download and install Android Studio
        b. Run commands in command line on a different IDE, i.e VSCode, to open project in Android Studio
        c. Create and/or choose the Android App configuration
        d. Install desired emulation device
        e. Resync any dependencies needed and run the build.
            
