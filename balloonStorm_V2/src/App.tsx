import { Redirect, Route, RouteComponentProps } from 'react-router-dom';
import { IonApp, IonRouterOutlet } from '@ionic/react';
import { IonReactRouter } from '@ionic/react-router';
import Home from './pages/Home';
import ble_page from './pages/ble_page';
import MainMenu from './pages/mainMenu';
import AlternateMainMenu from './pages/alternateMainMenu';  // Added by Ryan
import LeaderBoard from './pages/leaderboard';
import CombinedMenu2 from './pages/combinedMenu2'; // Added by Ryan
import { Login } from './pages/Login';

/* Core CSS required for Ionic components to work properly */
import '@ionic/react/css/core.css';

/* Basic CSS for apps built with Ionic */
import '@ionic/react/css/normalize.css';
import '@ionic/react/css/structure.css';
import '@ionic/react/css/typography.css';

/* Optional CSS utils that can be commented out */
import '@ionic/react/css/padding.css';
import '@ionic/react/css/float-elements.css';
import '@ionic/react/css/text-alignment.css';
import '@ionic/react/css/text-transformation.css';
import '@ionic/react/css/flex-utils.css';
import '@ionic/react/css/display.css';

/* Theme variables */
import './theme/variables.css';

import RealmApolloProvider from './graphql/RealmApolloProvider';
import {RealmAppProvider, useRealmApp} from './Realm'; //Added by Joey
export const APP_ID = 'balloonstorm-pbqmh'; //MongoDB Realm app-id

interface Props {
  Component: React.FC<RouteComponentProps>;
  path: string;
  exact?: boolean;
}

const AuthRoute = ({ Component, path, exact = true }: Props): JSX.Element => {
  const app = useRealmApp();
  const isAuthed = !!app.currentUser;
  return (
    <Route
      exact={exact}
      path={path}
      render={(props: RouteComponentProps) =>
        isAuthed ? <Component {...props} /> : <Redirect to="/" />
   }
   />
  );
};

/* <Route exact path="/mainMenu">
          <MainMenu />
        </Route>
        <Route exact path="/">
          <Redirect to="/mainMenu" />
        </Route>
*/
const App: React.FC = () => (
  <RealmAppProvider appId={APP_ID}>
    <RealmApolloProvider>
    <IonApp>
      <IonReactRouter>
        <IonRouterOutlet>
          <Route exact path="/combinedMenu2">
            <CombinedMenu2 />
          </Route>
          <Route exact path="/alternateMainMenu">
            <AlternateMainMenu />
          </Route>
          <Route exact path="/" component={Login}/>

          <Route exact path="/ble_page" component={ble_page}></Route>
          <Route exact path="/mainMenu" component={MainMenu}></Route>
          <Route exact path="/leaderboard" component={LeaderBoard}></Route>
          <Route exact path="/alternateMainMenu" component={AlternateMainMenu}></Route>
          <Route exact path="/combinedMenu2" component={CombinedMenu2}></Route>
        </IonRouterOutlet>
      </IonReactRouter>
    </IonApp>
    </RealmApolloProvider>
  </RealmAppProvider>
);

export default App;
