import { IonContent } from '@ionic/react';
/*
NOTE: Adding in the above import because: Typescript treats files without import/exports as legacy script files. 
As such files are not modules and any definitions they have get merged in the global namespace. isolatedModules forbids such files.
Adding any import or export to a file makes it a module and the error disappears.
*/

/*
import Realm from "realm";

export async function testDB(){
    var realm: any;
    const highScoresSchema = {
        name: "highScores",
        properties: {
            _id: "objectId",
            playerName: "string",
            score: "int",
            sessionStart: "date"
        },
        primaryKey: "_id",
    };
  
    try {
        realm = await Realm.open({
            path: "balloonStormGameData",
            schema: [highScoresSchema],
        });
        return realm;
    } catch (err: any) {
        console.error("Failed to open the realm", err.message);
    };

    let dateTime = new Date();
    let gameSession;

    realm.write(() => {
        gameSession = realm.create("highScores", {playerName: "JohnD", score: 1000, sessionStart: dateTime})
    });

    const scores = realm.objects("highScores");
    console.log(scores);

    realm.close();
}
*/