// Author: Ryan Pfanner
// Documentation of 'alternateMainMenu':

    //
    // When using Ionic/REact tags, you must import them as shows in the import section below.
    // 'AlternateMainMenu' is my main function that I also use in App.tsx
    // IonList/IonItem/IonButton -> To display a list of the menu buttons.
    // -----
    // IonMenu -> Creates a side menu with properties such as: 'overlay' and 'primary'
    // -----
    // Side menu is seperate from main page. 
    // Main page: noted by <IonPage> </IonPage> tags.
    // Slide Side Menu: noted by <IonMenu> </IonMenu> tags.
    // -----
    // The <IonRouterOutlet> tag allows the <IonMenu> to appear. The Id's must match eachother.
    //

// End of 'alternateMainMenu' Documentation

// IMPORT SECTION
import { IonContent, IonHeader, IonPage, IonTitle, IonToolbar, 
    IonInput, IonItem, IonLabel, IonText, IonButton, IonCard, 
    IonCardContent, IonList, IonApp, IonMenu, IonRouterOutlet, 
    IonIcon, IonButtons, IonMenuButton} from '@ionic/react';
import { stringify } from 'querystring';
import React, { useState, useRef } from 'react';
import './alternateMainMenu.css';
import LeaderBoard from './leaderboard';
// 
const AlteranteMainMenu: React.FC = () => {

    const [showLeaderBoard, setLeaderBoardSwitch] = useState<boolean>();
    const [playerName, setPlayerName] = useState<string>(); 
    const nameInputRef = useRef<HTMLIonInputElement>(null);
 

    const setName = () => {
        const enteredName = nameInputRef.current!.value;
        if(!enteredName){
        return;
        }
        setPlayerName(JSON.stringify(enteredName));
    };

    const clearName = () => {
        setPlayerName('');
    };

    const leaderBoardSwitch = () => {
        console.log("switching leaderboard variable from " + showLeaderBoard);
        if (!showLeaderBoard){
            setLeaderBoardSwitch(true);
        }
        else {
            setLeaderBoardSwitch(false);
        }
        console.log(" to " + showLeaderBoard)
    };

    return (
    <IonApp>
        <IonPage>
            <IonToolbar color="primary">
                <IonButtons slot="start">
                    <IonMenuButton color="light"></IonMenuButton>
                </IonButtons>
                <IonTitle>Balloon Storm! Run!</IonTitle>
            </IonToolbar>
            <IonContent>
                <IonCard>
                    {(!playerName && !showLeaderBoard) &&
                    (<IonCardContent>
                    <div className="inputName">
                        <IonItem>
                            <IonInput ref={nameInputRef} id="userName" inputMode="text" maxlength={15} placeholder="Enter player name"></IonInput>
                        </IonItem>
                        <IonButton onClick={setName} id="submit" type="submit" size="large">Submit</IonButton>
                    </div>
                    </IonCardContent>)}
                    {showLeaderBoard &&
                    (<IonCardContent>
                        <IonHeader>High Scores</IonHeader>
                        <IonItem>                            
                            <IonList>
                                <IonItem>1.     Dumbledore          5000</IonItem>
                                <IonItem>2.     Harry Potter        2500</IonItem>
                                <IonItem>3.     Hermione Granger    1000</IonItem>
                                <IonItem>4.     Ron Weasley         500</IonItem>
                                <IonItem>5.     Draco Malfoy        100</IonItem>
                            </IonList>
                        </IonItem>
                    </IonCardContent>)}
                </IonCard>
            </IonContent>
        </IonPage>
        <IonMenu contentId="side-menu" type="overlay">
            <IonContent fullscreen>
                <IonHeader>
                    <IonToolbar color="tertiary">
                        {playerName && (<IonCardContent id="pageContent">
                        <IonTitle>Wizard: {playerName}</IonTitle>
                        </IonCardContent>)}
                    </IonToolbar>
                </IonHeader>
                <IonList>
                    <IonCardContent >
                    <div id="menuButtons">
                        <IonItem><IonButton color="tertiary">Start</IonButton></IonItem>
                        <IonItem><IonButton color="tertiary" onClick={leaderBoardSwitch}>Leaderboard</IonButton></IonItem>
                        <IonItem><IonButton color="tertiary">Pair Your Wand</IonButton></IonItem>
                        <IonItem><IonButton color="tertiary" onClick={clearName}>Change Name</IonButton></IonItem>
                        <IonItem><IonButton color="tertiary">Settings</IonButton></IonItem>
                        <IonItem><IonButton color="tertiary">Quit</IonButton></IonItem>
                    </div>
                    </IonCardContent>
                </IonList>
            </IonContent>
        </IonMenu>
        <IonRouterOutlet id="side-menu"></IonRouterOutlet>
    </IonApp>

    );        
    
};

export default AlteranteMainMenu;