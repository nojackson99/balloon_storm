// Author: Ryan Pfanner
// co-Author(s): 
//
// objrender.js DOCUMENTATION (Below)
/*
  Notes: 
*/
//
import React, { useState } from 'react';
import { DefaultXRControllers, ARCanvas, Interactive } from '@react-three/xr'
import { Text } from '@react-three/drei'
import ReactDOM from 'react-dom'
import { Canvas, useFrame } from "@react-three/fiber";
// Styling
import './webcamDisplay.css'

export function WebCam() {
	// Hooks for determining wehter the video is playing or not palying.
	const [playing, setPlaying] = useState(false);
  
	// Sizes used by the video element when displaying the video (camera size)
	const HEIGHT = 1000; 
	const WIDTH = 1000;
  
	// BEGIN VIDEO FUNCTION
	const startVideo = () => {
    	// Must ALWAYS be true.
		setPlaying(true);
		// To get the video stream footage.
		navigator.getUserMedia(
			{
				// Object for the video stream which takes a reference of a class name.
				video: true,
			},
			// Stream displays webcam footage
			(stream) => {
				let video = document.getElementsByClassName('app__videoFeed')[0];
				// if video is running...
				if (video) {
					// 
					video.srcObject = stream;
				}
			},
			// Error Catch
			(err) => console.error(err)
		);
	};
	// STOP VIDEO FUNCTION
	const stopVideo = () => {
		// Must ALWAYS be false.
		setPlaying(false);
		// Setting video object to ZERO when user stops the video.
		let video = document.getElementsByClassName('app__videoFeed')[0];
		video.srcObject.getTracks()[0].stop();
	};
	
  return ( 
    <div className="app">
			<div className="app__container">
				<video
					// Video tag characteristics
					height={HEIGHT}
					width={WIDTH}
					muted
					autoPlay
					className="app__videoFeed"
				>	
				</video>
			</div>
			<div className="app__input">
				{playing ? (
					// Stop Button
					<button onClick={stopVideo}>Stop</button>
				) : (
					// Start Button
					<button onClick={startVideo}>Start</button>
				)}
			</div>
		</div>
	
  );
}

