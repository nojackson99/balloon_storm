// File Created 10/2/2021
// Author: Ryan Pfanner
// Co-Authors: Joe Ciriello, Duncan Fox, Aiden Smith
// Documentation of 'combinedMainMenu' Menu components:

    //
    // When using Ionic/REact tags, you must import them as shows in the import section below.
    // 'AlternateMainMenu' is my main function that I also use in App.tsx
    // IonList/IonItem/IonButton -> To display a list of the menu buttons.
    // -----
    // IonMenu -> Creates a side menu with properties such as: 'overlay' and 'primary'
    // -----
    // Side menu is seperate from main page. 
    // Main page: noted by <IonPage> </IonPage> tags.
    // Slide Side Menu: noted by <IonMenu> </IonMenu> tags.
    // -----
    // The <IonRouterOutlet> tag allows the <IonMenu> to appear. The Id's must match eachother.
    // 

// End of 'alternateMainMenu' Documentation

// START of: IMPORT SECTION 
    // MENU IMPORT SECTION (Ryan Pfanner)
    import { IonContent, IonHeader, IonPage, IonTitle, IonToolbar, 
        IonInput, IonItem, IonLabel, IonText, IonButton, IonCard, 
        IonCardContent, IonList, IonApp, IonMenu, IonRouterOutlet, 
        IonIcon, IonButtons, IonMenuButton, IonLoading} from '@ionic/react';
    import { stringify } from 'querystring';
    import React, { useState, useRef } from 'react';
    import './combinedMenu2.css';
    // BLE IMPORT SECTION (Duncan Fox)
    import { play, clipboard,  person, colorWand, bluetooth, settings, exit } from 'ionicons/icons';
    import ExploreContainer from '../components/ExploreContainer';
    import Ble_Connector from './ble_page';
    import BleWandDiagnosticView from '../components/BleWandDiagnosticView';
    import Ble_Canvas from '../components/Ble_Canvas';
    import {CanvasData} from '../components/Ble_Canvas_Support';
    import { WandAccelData, WandButtonData } from '../components/BleInterface';
    // three.js Imports (Ryan Pfanner)
    import * as THREE from '../components/three.js-master/build/three.module.js'; // import * as THREE from 
    import { OrbitControls } from '../components/three.js-master/examples/jsm/controls/OrbitControls.js'; // import { OrbitControls } from 
    import Stats from '../components/three.js-master/examples/jsm/libs/stats.module.js'; // import { Stats } from 
    import { ARButton } from '../components/three.js-master/examples/jsm/webxr/ARButton.js'; // import { ARButton } from 
    // OBJECT RENDER SECTION (Aiden Smith, Ryan Pfanner)
        //import ObjRender from '../components/objrender';  // COMMENTED OUT - 11/1/2021
        // Component code for the above import => {showPlaySpace && (<App></App>)}
    import App2 from '../components/objRender2';
    import styled from 'styled-components';
    // AR SECTION (Ryan Pfanner)
    import { ARCanvas, Interactive, DefaultXRControllers } from '@react-three/xr'
        // OTHER
    import { WebCam } from './webcamDisplay';
    import { useRealmApp } from '../Realm';
    import useScores from '../graphql/useHighScores';
    import { ScoreItem } from '../components/scoreItem';

    //Imports for timer
    import Countdown from 'react-countdown';
    
    // Android permissions (Noah Jackson)
    import permissions from '../components/PermissionServices';

        import ReactDOM from 'react-dom'
import ObjRender from '../components/objrender';
//import ObjRender from '../components/objrender';
import Game_Canvas from '../components/Game_Canvas';
//
import hitBox from '../components/Game_Canvas';
import { BalloonPath } from '../components/Balloon_Objects';
import Basic_Balloon from '../components/Basic_Balloon';

//End of: IMPORT SECTION

// -------------------------------------------------------------------------------------------------------------------------------------------------------------


//Start styled Ionic components 

// Start (Joeseph Ciriello)
const LeaderBoardList = styled(IonList)`
    width: auto;
    margin-top: 0%;
    display: inline;
    padding-left: 35%;
`;

const LeaderBoardContainer = styled(IonCardContent)`
    width: auto;
    align-self: center;
`;

const MenuCard = styled(IonCard)`
    width: 50%;
    align-self: center;
    margin: auto;
`;

const GameTimer = styled(IonTitle)`
    align-self: center;
    padding-left: 45%;
`;

const GameHeader = styled(IonToolbar)`
    display: inline;
`
// End (Joseph)

// Start (Noah Jackson)
const SettingsContainer = styled(IonCardContent)`
    width: auto;
    align-self: center;
`;

const ButtonColor = styled(IonButton)`
    --background: purple;
`
// End (Noah Jackson)

//End styled Ionic components (Joeseph Ciriello)

const CombinedMenu2: React.FC = () => {

    const app = useRealmApp();
    const [currentProject] = useState(app.currentUser);
    const { scores, addScore, loading } = useScores(currentProject);
    
        //START OF: BLE Callback Code (Duncan Fox) ==================================
        function HitBoxCallback(data: BalloonPath){
            setHitBoxData(data);
        }
        const [hitBoxData, setHitBoxData] = useState<BalloonPath>(new BalloonPath()); // variable

        function WandAccelCallback(data: WandAccelData){
            setWandAccelData(data);
        }
        const [wandAccelData, setWandAccelData] = useState<WandAccelData>(new WandAccelData());  // variable

        function WandButtonCallback(data: WandButtonData){
            setWandButtonData(data);
        }
        const [wandButtonData, setWandButtonData] = useState<WandButtonData>(new WandButtonData());  // variable

        function CanvasCallback(data: CanvasData){
            setCanvasData(data);
        }
        const [canvasData, setCanvasData] = useState<CanvasData>(new CanvasData());  // variable
    
        //END 
    
        // START of: AR Space Code (Ryan Pfanner)
    const [showPlaySpace, setPlaySwitch] = useState<boolean>();
    const startPlay = () => {
        console.log("switching playButton variable from " + showPlaySpace);
        if (!showPlaySpace){
            ReactDOM.render(
                // 60,000 = 1 minute or 60 seconds
                <Countdown date={Date.now() + 80000} /*renderer={timerRenderer}*//>,
                document.getElementById('timer'))
            setPlaySwitch(true);
            hitBoxData.totalPoints = 0;
            hitBoxData.totalHits = 0;
            hitBoxData.redHits = 0;
            hitBoxData.blueHits = 0;
            hitBoxData.yellowHits = 0;
            HitBoxCallback(hitBoxData);
        }
        else {
            return
        }
        console.log(" to " + showPlaySpace)
    };

    const stopPlay = () => {
        console.log("switching playButton variable from " + showPlaySpace);
        if (showPlaySpace){
            ReactDOM.render(
                <GameTimer id="timer"></GameTimer>,
                document.getElementById('timer'))
            setPlaySwitch(false);
        }
        else {
            return
        }
        console.log(" to " + showPlaySpace)
    }
    // END of: AR Space Code (Ryan Pfanner)

    // START of: Player Name Code (Joseph Ciriello)
    const [playerName, setPlayerName] = useState<string>(); 

    const nameInputRef = useRef<HTMLIonInputElement>(null);
    const setName = () => {
        const enteredName = nameInputRef.current!.value;
        if(!enteredName){
        return;
        }
        setPlayerName(JSON.stringify(enteredName));
    };
    const clearName = () => {
        setPlayerName(``);
    }
    // END of: Player Name Code (Joseph Ciriello)

    //START of: toggle leaderboard on/off (Joseph Ciriello)
    const [showLeaderBoard, setLeaderBoardSwitch] = useState<boolean>();
    const leaderBoardSwitch = () => {
        console.log("switching leaderboard variable from " + showLeaderBoard);
        if (!showLeaderBoard){
            setLeaderBoardSwitch(true);
        }
        else {
            setLeaderBoardSwitch(false);
        }
        console.log(" to " + showLeaderBoard);
    };
    //End of: toggle leaderboard on/off
    //Start of: toggle test page to insert scores to database
    const [showTestBoard, setShowTestBoard] = useState<boolean>();
    const testBoardSwitch = () => {
        console.log("switching leaderboard variable from " + showTestBoard);
        if (!showTestBoard){
            setShowTestBoard(true);
        }
        else {
            setShowTestBoard(false);
        }
        console.log(" to " + showTestBoard);
    };
    //Start of: toggle test page to insert scores to database
    //START of: toggle BLE test on/of    
    //END of: toggle leaderboard on/off (Joseph Ciriello)

    //START of: toggle BLE test on/of
    const [showBLETest, setShowBLESwitch] = useState<boolean>();
    const showBLESwitch = () => {
        console.log("switching showBLE variable from " + showBLETest);
        if(!showBLETest){
            setShowBLESwitch(true);
        }
        else{
            setShowBLESwitch(false);
        }
        console.log(" to " + showBLETest);
    }
    //END of: toggle BLE test on/of
    //START of: mutating testScore
    const [testScore, setTestScore] = useState(0);

    //START of: add highScore document
    const addNewScore = async () => {
        if(testScore == 0){
            return;
        }
        await addScore({playerName: playerName, gameScore: testScore});
    };
    //END of: add highScore document
    //START of: android permissions functions (Noah Jackson)
    const cameraPermission = () => { //this is for testing correct implementation of import and function calls
        permissions.requestCameraPermission();
    }
    const fineLocation = () => {
        permissions.requestFineLocation();
    }

    //toggle settings on/off
    const [showSettings, setSettingsSwitch] = useState<boolean>();
    const settingsSwitch = () => {
        console.log("switching settings variable from " + showSettings);
        if(!showSettings){
            setSettingsSwitch(true);
        }
        else{
            setSettingsSwitch(false);
        }
        console.log(" to " + showSettings);
    }

    //END OF: android permissions (Noah Jackson)

    //const Completion = () => <span>Game over!</span>

    /*const timerRenderer = ({ seconds, completed }) => {
        if (completed){
            return <Completion />
        } else {
            return <span>{seconds}</span>
        }
    };*/

    const callTimer = () => {
        ReactDOM.render(
            <Countdown date={Date.now() + 60000} /*renderer={timerRenderer}*//>,
            document.getElementById('timer')
        );
    };

    return (
    <IonApp>
        <IonPage>
            <GameHeader color="primary">
                <IonButtons slot="start">
                    <IonMenuButton color="light"></IonMenuButton>
                </IonButtons>
                <IonTitle>Balloon Storm! Run!</IonTitle>
                <GameTimer id="timer" slot="end"></GameTimer>
            </GameHeader>
            <IonContent>
                <MenuCard>
                    {(!playerName && !showLeaderBoard && !showBLETest) &&
                    (<IonCardContent>
                    <div className="inputName">
                        <IonItem>
                            <IonInput ref={nameInputRef} id="userName" inputMode="text" maxlength={15} placeholder="Enter player name"></IonInput>
                        </IonItem>
                        <IonButton onClick={setName} id="submit" type="submit" size="large">Submit</IonButton>
                    </div>
                    </IonCardContent>)}

                    {/* {showTestBoard && 
                    (<IonItem><IonTitle>Current score: {testScore}</IonTitle>
                        <IonButton onClick={() => setTestScore(testScore + 1000)} >Add 1000</IonButton>
                        <IonButton onClick={() => setTestScore(testScore + 100)}>Add 100</IonButton>
                        <IonButton onClick={() => setTestScore(testScore + 10)}>Add 10</IonButton>
                        <IonButton onClick={addNewScore}>Submit Test Score</IonButton>
                     </IonItem>
                    )} */}

                    {showLeaderBoard &&
                    (<LeaderBoardContainer>
                            <IonHeader>High Scores</IonHeader>
                            <div className="leaderboardContainer">                    
                                <IonItem>                       
                                    <LeaderBoardList>
                                        {loading ? <IonLoading isOpen={loading} /> : null}
                                        {scores.map((score: any) => (
                                            <ScoreItem key={parseInt(score._id)}{...score}></ScoreItem>
                                        ))}
                                        <IonItem>                            
                                            <IonList>
                                                <IonItem>1.     Dumbledore:          10000</IonItem>
                                                <IonItem>2.     Voldemort:           9500</IonItem>
                                                <IonItem>3.     Severus Snape:       8500</IonItem>
                                                <IonItem>4.     Harry Potter:        8000</IonItem>
                                                <IonItem>5.     Minerva McGonagall:  7000</IonItem>
                                                <IonItem>6.     Hermione Granger:    6500</IonItem>
                                                <IonItem>7.     Ron Weasley:         5000</IonItem>
                                                <IonItem>8.     Draco Malfoy:        3500</IonItem>
                                                <IonItem>9.     Luna Lovegood:       2000</IonItem>
                                                <IonItem>10.    Neville Longbottom:  1000</IonItem>
                                                <IonItem>                                </IonItem>
                                                <IonItem>11. {playerName}: {hitBoxData.totalPoints}</IonItem>
                                            </IonList>
                                        </IonItem>
                                    </LeaderBoardList>
                                </IonItem> 
                            </div>                      
                    </LeaderBoardContainer>)}

                    {/* settings toggle */}
                    {showSettings &&
                    <SettingsContainer>
                        <IonTitle>Permissions</IonTitle>
                        <IonList>
                            <IonItem>
                                {/* button to request camera permission */}
                                 <IonButton color="tertiary" onClick={cameraPermission}>Camera</IonButton>
                            </IonItem>
                            <IonItem>
                                  <IonButton color="tertiary" onClick={fineLocation}>Fine_Location</IonButton>
                            </IonItem>
                        </IonList>
                    </SettingsContainer>}
                    
                    {showBLETest && (
                    <div>
                    <Ble_Connector onAccelStateChanged={WandAccelCallback} onButtonStateChanged={WandButtonCallback} wandAccelData={wandAccelData} wandButtonData={wandButtonData}></Ble_Connector>
                    <BleWandDiagnosticView wandAccelData={wandAccelData} wandButtonData={wandButtonData}/>
                    <Ble_Canvas wandAccelData={wandAccelData} wandButtonData={wandButtonData} onCanvasStateChanged={CanvasCallback} canvasData={canvasData}/>
                    </div>
                    )}
                </MenuCard>  
                {showPlaySpace && (<Game_Canvas wandAccelData={wandAccelData} wandButtonData={wandButtonData} onCanvasStateChanged={CanvasCallback} canvasData={canvasData} onHitBoxCallback={HitBoxCallback} balloon={hitBoxData}/>)}
            </IonContent>
        </IonPage>
        <IonMenu contentId="side-menu" type="overlay">
            <IonContent fullscreen>
                <IonHeader>
                    <IonToolbar color="tertiary">
                        {playerName && (<IonCardContent id="pageContent">
                        <IonTitle>Wizard: {playerName}</IonTitle>
                        </IonCardContent>)}
                    </IonToolbar>
                </IonHeader>
                <IonList>
                    <IonCardContent>
                    <div id="menuButtons">
                        <IonItem>
                            <IonIcon color="tertiary" slot="start" icon={ play } ></IonIcon>
                            <IonButton color="tertiary" onClick={startPlay}>Play</IonButton>
                        </IonItem>
                        <IonItem>
                            <IonIcon color="tertiary" slot="start" icon={ person } ></IonIcon>
                            <IonButton color="tertiary" onClick={clearName}>Change Name</IonButton>
                        </IonItem>
                        <IonItem>
                            <IonIcon color="tertiary" slot="start" icon={ clipboard } ></IonIcon>
                            <IonButton color="tertiary" onClick={leaderBoardSwitch}>Leaderboard</IonButton>
                        </IonItem>
                        <ExploreContainer />
                        <IonItem>
                            <IonIcon color="tertiary" slot="start" icon={ colorWand } ></IonIcon>
                            <Ble_Connector onAccelStateChanged={WandAccelCallback} onButtonStateChanged={WandButtonCallback} wandAccelData={wandAccelData} wandButtonData={wandButtonData} />
                        </IonItem>
                        <IonItem>
                            <IonIcon color="tertiary" slot="start" icon={ bluetooth } ></IonIcon>
                            <IonButton color="tertiary" onClick={showBLESwitch}>Go Test BLE</IonButton>
                        </IonItem>
                        <IonItem>
                            <IonIcon color="tertiary" slot="start" icon={ settings } ></IonIcon>
                            <IonButton color="tertiary" onClick={settingsSwitch}>Settings</IonButton>
                        </IonItem>
                        <IonItem>
                        <IonIcon color="tertiary" slot="start" icon={ exit } ></IonIcon>
                            <IonButton color="tertiary" onClick={stopPlay}>Quit</IonButton>
                        </IonItem>
                    </div>
                    </IonCardContent>
                </IonList>
            </IonContent>
        </IonMenu>
        <IonRouterOutlet id="side-menu"></IonRouterOutlet>
    </IonApp>

    );        
    
};

export default CombinedMenu2;