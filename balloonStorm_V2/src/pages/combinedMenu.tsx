// File Created 10/2/2021
// Author: Ryan Pfanner
// Co-Authors: Joe Ciriello, Duncan Fox, Aiden Smith
// Documentation of 'combinedMainMenu' Menu components:

    //
    // When using Ionic/REact tags, you must import them as shows in the import section below.
    // 'AlternateMainMenu' is my main function that I also use in App.tsx
    // IonList/IonItem/IonButton -> To display a list of the menu buttons.
    // -----
    // IonMenu -> Creates a side menu with properties such as: 'overlay' and 'primary'
    // -----
    // Side menu is seperate from main page. 
    // Main page: noted by <IonPage> </IonPage> tags.
    // Slide Side Menu: noted by <IonMenu> </IonMenu> tags.
    // -----
    // The <IonRouterOutlet> tag allows the <IonMenu> to appear. The Id's must match eachother.
    // 

// End of 'alternateMainMenu' Documentation

// START of: IMPORT SECTION 
    // MENU IMPORT SECTION (Ryan Pfanner)
    import { IonContent, IonHeader, IonPage, IonTitle, IonToolbar, 
        IonInput, IonItem, IonLabel, IonText, IonButton, IonCard, 
        IonCardContent, IonList, IonApp, IonMenu, IonRouterOutlet, 
        IonIcon, IonButtons, IonMenuButton} from '@ionic/react';
    import { stringify } from 'querystring';
    import React, { useState, useRef } from 'react';
    import './combinedMenu2.css';
    // BLE IMPORT SECTION (Duncan Fox)
    import { play, clipboard,  person, colorWand, bluetooth, settings, exit } from 'ionicons/icons';
    import ExploreContainer from '../components/ExploreContainer';
    import Ble_Connector from './ble_page';
    import BleWandDiagnosticView from '../components/BleWandDiagnosticView';
    import Ble_Canvas from '../components/Ble_Canvas';
    import {CanvasData} from '../components/Ble_Canvas_Support';
    import { WandAccelData, WandButtonData } from '../components/BleInterface';
    // three.js Imports (Ryan Pfanner)
    import * as THREE from '../components/three.js-master/build/three.module.js'; // import * as THREE from 
    import { OrbitControls } from '../components/three.js-master/examples/jsm/controls/OrbitControls.js'; // import { OrbitControls } from 
    import Stats from '../components/three.js-master/examples/jsm/libs/stats.module.js'; // import { Stats } from 
    import { ARButton } from '../components/three.js-master/examples/jsm/webxr/ARButton.js'; // import { ARButton } from 
    // OBJECT RENDER SECTION (Aiden Smith, Ryan Pfanner)
        //import ObjRender from '../components/objrender';  // COMMENTED OUT - 11/1/2021
        // Component code for the above import => {showPlaySpace && (<App></App>)}
    import App2 from '../components/objRender2';
    import styled from 'styled-components';
    // AR SECTION (Ryan Pfanner)
    import { ARCanvas } from '@react-three/xr'
        // OTHER
        import { WebCam } from './webcamDisplay';

//End of: IMPORT SECTION

// -------------------------------------------------------------------------------------------------------------------------------------------------------------


//Start styled Ionic components (Joeseph Ciriello)
const LeaderBoardList = styled(IonList)`
    width: auto;
    margin-top: 0%;
    display: inline;
    padding-left: 35%;
`;

const LeaderBoardContainer = styled(IonCardContent)`
    width: auto;
    align-self: center;
`;

const MenuCard = styled(IonCard)`
    width: 50%;
    align-self: center;
    margin: auto;
`;
//End styled Ionic components (Joeseph Ciriello)

const CombinedMenu: React.FC = () => {
    
        //START OF: BLE Callback Code (Duncan Fox) ==================================
    
        function WandAccelCallback(data: WandAccelData){
            setWandAccelData(data);
          }
          function WandButtonCallback(data: WandButtonData){
            setWandButtonData(data);
          }
          function CanvasCallback(data: CanvasData){
            setCanvasData(data);
          }
        
          const [wandAccelData, setWandAccelData] = useState<WandAccelData>(
            new WandAccelData()
          );
          const [wandButtonData, setWandButtonData] = useState<WandButtonData>(new WandButtonData());
          const [canvasData, setCanvasData] = useState<CanvasData>(new CanvasData());
    
        //END 
    
        // START of: AR Space Code (Ryan Pfanner)
    const [showPlaySpace, setPlaySwitch] = useState<boolean>();
    const playSwitch = () => {
        console.log("switching playButton variable from " + showPlaySpace);
        if (!showPlaySpace){
            setPlaySwitch(true);
        }
        else {
            setPlaySwitch(false);
        }
        console.log(" to " + showPlaySpace)
    };
    // END of: AR Space Code (Ryan Pfanner)

    // START of: Player Name Code (Joseph Ciriello)
    const [playerName, setPlayerName] = useState<string>(); 

    const nameInputRef = useRef<HTMLIonInputElement>(null);
    const setName = () => {
        const enteredName = nameInputRef.current!.value;
        if(!enteredName){
        return;
        }
        setPlayerName(JSON.stringify(enteredName));
    };
    const clearName = () => {
        setPlayerName('');
    }
    // END of: Player Name Code (Joseph Ciriello)

    //START of: toggle leaderboard on/off (Joseph Ciriello)
    const [showLeaderBoard, setLeaderBoardSwitch] = useState<boolean>();
    const leaderBoardSwitch = () => {
        console.log("switching leaderboard variable from " + showLeaderBoard);
        if (!showLeaderBoard){
            setLeaderBoardSwitch(true);
        }
        else {
            setLeaderBoardSwitch(false);
        }
        console.log(" to " + showLeaderBoard);
    };
    //END of: toggle leaderboard on/off (Joseph Ciriello)
    const [showBLETest, setShowBLESwitch] = useState<boolean>();
    const showBLESwitch = () => {
        console.log("switching showBLE variable from " + showBLETest);
        if(!showBLETest){
            setShowBLESwitch(true);
        }
        else{
            setShowBLESwitch(false);
        }
        console.log(" to " + showBLETest);
    }
    //START of: toggle BLE test on/of
    return (
    <IonApp>
        <IonPage>
            <IonToolbar color="primary">
                <IonButtons slot="start">
                    <IonMenuButton color="light"></IonMenuButton>
                </IonButtons>
                <IonTitle>Balloon Storm! Run!</IonTitle>
            </IonToolbar>
            <IonContent>
                <MenuCard>
                    {(!playerName && !showLeaderBoard && !showBLETest) &&
                    (<IonCardContent>
                    <div className="inputName">
                        <IonItem>
                            <IonInput ref={nameInputRef} id="userName" inputMode="text" maxlength={15} placeholder="Enter player name"></IonInput>
                        </IonItem>
                        <IonButton onClick={setName} id="submit" type="submit" size="large">Submit</IonButton>
                    </div>
                    </IonCardContent>)}
                    {showLeaderBoard &&
                    (<LeaderBoardContainer>
                            <IonHeader>High Scores</IonHeader>
                            <div className="leaderboardContainer">                    
                                <IonItem>                       
                                    <LeaderBoardList>
                                        <div className="leaderboardContent">
                                            <IonItem>1.     Dumbledore          5000</IonItem>
                                            <IonItem>2.     Harry Potter        2500</IonItem>
                                            <IonItem>3.     Hermione Granger    1000</IonItem>
                                            <IonItem>4.     Ron Weasley         500</IonItem>
                                            <IonItem>5.     Draco Malfoy        100</IonItem>
                                        </div>
                                    </LeaderBoardList>
                                </IonItem> 
                            </div>                      
                    </LeaderBoardContainer>)}
                    {showBLETest && (
                    <div>
                    <Ble_Connector onAccelStateChanged={WandAccelCallback} onButtonStateChanged={WandButtonCallback} wandAccelData={wandAccelData} wandButtonData={wandButtonData}></Ble_Connector>
                    <BleWandDiagnosticView wandAccelData={wandAccelData} wandButtonData={wandButtonData}/>
                    <Ble_Canvas wandAccelData={wandAccelData} wandButtonData={wandButtonData} onCanvasStateChanged={CanvasCallback} canvasData={canvasData}/>
                    </div>
                    )}
                </MenuCard>
                {showPlaySpace && (<WebCam></WebCam>)}
                {showPlaySpace && (<App2></App2>)}
            </IonContent>
        </IonPage>
        <IonMenu contentId="side-menu" type="overlay">
            <IonContent fullscreen>
                <IonHeader>
                    <IonToolbar color="tertiary">
                        {playerName && (<IonCardContent id="pageContent">
                        <IonTitle>Wizard: {playerName}</IonTitle>
                        </IonCardContent>)}
                    </IonToolbar>
                </IonHeader>
                <IonList>
                    <IonCardContent>
                    <div id="menuButtons">
                        <IonItem>
                            <IonIcon color="tertiary" slot="start" icon={ play } ></IonIcon>
                            <IonButton color="tertiary" onClick={playSwitch}>Play</IonButton>
                        </IonItem>
                        <IonItem>
                            <IonIcon color="tertiary" slot="start" icon={ person } ></IonIcon>
                            <IonButton color="tertiary" onClick={clearName}>Change Name</IonButton>
                        </IonItem>
                        <IonItem>
                            <IonIcon color="tertiary" slot="start" icon={ clipboard } ></IonIcon>
                            <IonButton color="tertiary" onClick={leaderBoardSwitch}>Leaderboard</IonButton>
                        </IonItem>
                        <ExploreContainer />
                        <IonItem>
                            <IonIcon color="tertiary" slot="start" icon={ colorWand } ></IonIcon>
                            <Ble_Connector onAccelStateChanged={WandAccelCallback} onButtonStateChanged={WandButtonCallback} wandAccelData={wandAccelData} wandButtonData={wandButtonData}/>
                        </IonItem>
                        <IonItem>
                            <IonIcon color="tertiary" slot="start" icon={ bluetooth } ></IonIcon>
                            <IonButton color="tertiary" onClick={showBLESwitch}>Go Test BLE</IonButton>
                        </IonItem>
                        <IonItem>
                            <IonIcon color="tertiary" slot="start" icon={ settings } ></IonIcon>
                            <IonButton color="tertiary">Settings</IonButton>
                        </IonItem>
                        <IonItem>
                        <IonIcon color="tertiary" slot="start" icon={ exit } ></IonIcon>
                            <IonButton color="tertiary" onClick={playSwitch}>Quit</IonButton>
                        </IonItem>
                    </div>
                    </IonCardContent>
                </IonList>
            </IonContent>
        </IonMenu>
        <IonRouterOutlet id="side-menu"></IonRouterOutlet>
    </IonApp>

    );        
    
};

export default CombinedMenu;