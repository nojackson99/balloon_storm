import { IonContent, IonHeader, IonPage, IonTitle, IonToolbar, 
  IonInput, IonItem, IonLabel, IonText, IonButton, IonCard, 
  IonCardContent, IonList, IonApp, IonMenu, IonRouterOutlet, 
  IonIcon, IonButtons, IonMenuButton} from '@ionic/react';import  ExploreContainer  from '../components/ExploreContainer';
import './balloonPage.css';
import * as THREE from '../components/three.js-master/build/three.module.js';
import React, { useState, useRef } from 'react';


const BalloonPage: React.FC = () => {
    
  const [playerName, setPlayerName] = useState<string>(); 

  const nameInputRef = useRef<HTMLIonInputElement>(null);
  const setName = () => {
      const enteredName = nameInputRef.current!.value;
      if(!enteredName){
      return;
      }
      setPlayerName(JSON.stringify(enteredName));
  };
  const clearName = () => {
      setPlayerName('');
  }

  return (
  <IonApp>
      <IonPage>
          <IonToolbar color="primary">
              <IonButtons slot="start">
                  <IonMenuButton color="light"></IonMenuButton>
              </IonButtons>
              <IonTitle>Balloon Storm! Run!</IonTitle>
          </IonToolbar>
          <IonContent>
              <IonCard>
                  {!playerName &&
                  (<IonCardContent>
                  <div className="inputName">
                      <IonItem>
                          <IonInput ref={nameInputRef} id="userName" inputMode="text" maxlength={15} placeholder="Enter player name"></IonInput>
                      </IonItem>
                      <IonButton onClick={setName} id="submit" type="submit" size="large">Submit</IonButton>
                  </div>
                  </IonCardContent>)}
              </IonCard>
              <IonButton routerLink="combinedMenu">Go Back</IonButton>
          </IonContent>
      </IonPage>
      <IonMenu contentId="side-menu" type="overlay">
          <IonContent fullscreen>
              <IonHeader>
                  <IonToolbar color="tertiary">
                      {playerName && (<IonCardContent id="pageContent">
                      <IonTitle>Wizard: {playerName}</IonTitle>
                      </IonCardContent>)}
                  </IonToolbar>
              </IonHeader>
              <IonList>
                  <IonCardContent >
                  <div id="menuButtons">
                      <IonItem><IonButton color="tertiary" id="startButton">Start</IonButton></IonItem>
                      <IonItem><IonButton color="tertiary">Leaderboard</IonButton></IonItem>
                      <IonItem><IonButton color="tertiary">Pair Your Wand</IonButton></IonItem>
                      <IonItem><IonButton color="tertiary">Settings</IonButton></IonItem>
                      <IonItem><IonButton color="tertiary">Quit</IonButton></IonItem>
                  </div>
                  </IonCardContent>
              </IonList>
          </IonContent>
      </IonMenu>
      <IonRouterOutlet id="side-menu"></IonRouterOutlet>
  </IonApp>

  );        
  
};

export default BalloonPage;