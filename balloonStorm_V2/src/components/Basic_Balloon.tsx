//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//Basic Balloon Component
//Author: Duncan Fox
//This component will render a basic balloon within the canvas.  For right now
//It's a cube.  That will change once meshes have been modifed.
//
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

import { BalloonPath } from './Balloon_Objects';

import React, { useRef, useState, useLayoutEffect } from "react";
//import * as THREE from './three.js-master/build/three';
import { Canvas, useFrame } from "@react-three/fiber";
import { OrbitControls, PerspectiveCamera } from "@react-three/drei";
import { CanvasData, accelAdjust } from "./Ble_Canvas_Support";

interface BalloonProps{
    balloonPath: BalloonPath

    //onHitBoxCallback:(Balloon:BalloonPath) => void;
}

const Basic_Balloon: React.FC<BalloonProps> = (props) => {

  

  return (
      <mesh position={[props.balloonPath.initialX, props.balloonPath.initialY, props.balloonPath.initialZ]} rotation={[0, 0, 0]}>
        <sphereBufferGeometry  args={[0.5, 16, 16]} />
        <meshStandardMaterial color={props.balloonPath.balloonColor} />

        
      </mesh>
  );
};
export default Basic_Balloon;
