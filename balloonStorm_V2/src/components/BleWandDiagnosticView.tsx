//+++++++++++++++++++++++++++++++++++++++++++++++++++
//BleWandDiagnosticView.tsx
//Author: Duncan Fox
//This file contains the a diagnostic viewer for raw data pulled from
//the wand's accelerometer and button characteristics.
//+++++++++++++++++++++++++++++++++++++++++++++++++++

import {useEffect, useState} from 'react';
import {IonButton} from "@ionic/react";
import {WandAccelData, WandButtonData} from './BleInterface';
import "./WandDiagnostic.css"

interface DiagnosticProps{
    wandAccelData: WandAccelData;
    wandButtonData: WandButtonData;
}


const  BleWandDiagnosticView: React.FC<DiagnosticProps> = (props) => {


const [buttonClass, setButtonClass] = useState("ButtonDefault");

return (

<div>

    <IonButton className={buttonClass}>    Test!    </IonButton>
    <table>
        <tbody>
            <td>Button State</td>
            <td>{props.wandButtonData.buttonStatus}</td>
        </tbody>
        <tbody>
            <td>Button Time</td>
            <td>{}</td>
        </tbody>
        <tbody>
            <td>Button Press</td>
            <td>{props.wandButtonData.spellColor}</td>
        </tbody>
        <tbody>
            <td>X State</td>
            <td>{props.wandAccelData.xQuat}</td>
        </tbody>
        <tbody>
            <td>Y State</td>
            <td>{props.wandAccelData.yQuat}</td>
        </tbody>
        <tbody>
            <td>Z State</td>
            <td>{props.wandAccelData.zQuat}</td>
        </tbody>
        <tbody>
            <td>Roll State</td>
            <td>{props.wandAccelData.wQuat}</td>
        </tbody>
    </table>
</div>


);


};
export default BleWandDiagnosticView;
