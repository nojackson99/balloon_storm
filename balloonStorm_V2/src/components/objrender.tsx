// Author: Aiden Smith
// co-Author(s): Ryan
//
// objrender.tsx DOCUMENTATION (Below)
/*
*/
//

// MAIN SECTION (Aiden Smith, Ryan Pfanner)
import React, {useRef, useState} from "react";
import * as THREE from '../components/three.js-master';
// RENDER SECTION (Aiden Smith)
import { Canvas, useFrame } from "@react-three/fiber";
import { OrbitControls, PerspectiveCamera, Box, MeshWobbleMaterial, Stars } from "@react-three/drei";
// AR SECTION (Ryan Pfanner)
import { ARCanvas, VRCanvas,  Interactive } from '@react-three/xr'
//
//const xr = navigator.xr;
import { WandAccelData, WandButtonData } from "./BleInterface";
import { BalloonPath } from "./Balloon_Objects";
import { CanvasData } from "./Ble_Canvas_Support";
import Basic_Balloon from "./Basic_Balloon";
import Wand_Reticle from "./Wand_Reticle";


//const { controllers } = useXR()
import { DefaultXRControllers, useXR } from '@react-three/xr'

interface GameProps {
  wandAccelData: WandAccelData;
  wandButtonData: WandButtonData;
  canvasData: CanvasData;


}


const ObjRender: React.FC<GameProps> = (props) => {    

  const { controllers } = useXR()

  let bPath = new BalloonPath();
  let bPath2 = new BalloonPath();
  let bPath3 = new BalloonPath();
  let bPath4 = new BalloonPath();

  bPath.initialX = 2;
  bPath.initialY = 2;
  bPath.initialZ = 0;

  bPath2.initialX = 1;
  bPath2.initialY = -1;
  bPath2.initialZ = 0;

  bPath3.initialX = 2;
  bPath3.initialY = -2;
  bPath3.initialZ = 0;

  bPath4.initialX = -2;
  bPath4.initialY = 2;
  bPath4.initialZ = 0;




  return (
      <ARCanvas>
        <DefaultXRControllers />
        <ambientLight intensity={0.1} />
        <directionalLight color="hotpink" position={[50, 50, 50]} />
        <spotLight position={[10,15,10]} angle={0.3} />
        <Wand_Reticle wandAccelData={props.wandAccelData} wandButtonData= {props.wandButtonData} canvasData={props.canvasData} />
        <Basic_Balloon balloonPath = {bPath} />



      </ARCanvas>    
  )
}

/* const SpinningBox = ( ) => {

}; */

export default ObjRender
