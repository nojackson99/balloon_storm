//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//BLE Support Functions.tsx
//Author: Duncan Fox
//This file contains all functions that are used to support BLE the BLE interpreter.
//This will include parser functions as well as keep alive and zeroing functions.
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

import {
  BleClient,
  numbersToDataView,
} from "@capacitor-community/bluetooth-le";
import { WandIds, WandAccelData, WandButtonData } from "./BleInterface";

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//keepAlive
//Author: Duncan Fox
//This function takes a WandId class and uses that information to write a value of 1
//to the keepAlive characteristic on the wand.  This does resets the time out on the wand
//so that the connection is maintained.
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
export function keepAlive(WandId: WandIds): void {
  BleClient.write(
    WandId.deviceID,
    WandId.IoService,
    WandId.ioKeepAlive,
    numbersToDataView([1])
  ).catch((error: any) => {
    console.log(error);
  });
  console.log(WandId.deviceID);
}

//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++==
//resetQuaternions Function
//Author: Duncan Fox
//This function takes a WandID class and extracts the device ID and UUIDs for the quaternion
//reset characteristic and the sensor service.  This information is then used to write a value
//of 1 to the Quaternion Reset characteristic on the wand.  This will reset all accelerometer
//values to zero, with the exception of pitch.
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

export function resetQuaternions(WandId: WandIds): void {
  BleClient.write(
    WandId.deviceID,
    WandId.SensorService,
    WandId.QuatReset,
    numbersToDataView([1])
  ).catch((error: any) => {
    console.log(error);
  });
}

function SingleReadSupport(wandAccelData: WandAccelData){
  return wandAccelData.yQuat;
}

async function SingleRead(wandId: WandIds){
  let param: number = await BleClient.read(wandId.deviceID, wandId.SensorService, wandId.Quaternions).then((value) => SingleReadSupport(parseTilt(value)));
  return param;
  
    
}
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//ParseTilt.tsx
//Author: Duncan Fox
//This function takes a DataView Object which contains information on accelerometer position in
//terms of 'Quaternions'.
//Once split up, this function will return a WandAccelData object with X, Y, Z, and W values
//populated.  This is used later to update the wand's pointing position.
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
export function parseTilt(data: DataView) {
  var tmpWandAccelData: WandAccelData = new WandAccelData();
  tmpWandAccelData.yQuat = data.getInt16(0, true);
  tmpWandAccelData.xQuat = data.getInt16(2, true);
  tmpWandAccelData.wQuat = data.getInt16(4, true);
  tmpWandAccelData.zQuat = data.getInt16(6, true);

  //const pos = conv.position([x, y, z, w]);

  let pitch = `Pitch: ${tmpWandAccelData.zQuat.toString()} `;
  let roll = `Roll: ${tmpWandAccelData.wQuat.toString()} `;

  //console.log(data);
  //console.log(
 //   `${pitch}${roll}(x, y): (${tmpWandAccelData.xQuat.toString()}, ${tmpWandAccelData.yQuat.toString()})`
  //);
  return tmpWandAccelData;
}



//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//parseButton
//Author: Duncan Fox
//the parseButton function takes a DataView object, a WandButtonData object, and a WandIDs object.
//WandIDs is used for the resetQuaternions and keepAlive functions.
//WandButtonData is used to store information on how and when the button is pressed so that it can be
//used to interact with the rest of the program.
//parseButton function will return a WandButtonData object.
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
export function parseButton(value: DataView, data: WandButtonData, motionData: WandAccelData, wandIds: WandIds) {
  data.buttonStatus = value.getInt8(0);
  let buttonSeconds = 0;
  let compare = 0;
  

  //Check if button is pressed.
  if (data.buttonStatus == 1) {
    data.timePress = Date.now();  //On first update, save time.
    compare = (data.timePress - data.timePressInitial) / 1000;


    if (data.timePressInitial == 0){
      data.timePressInitial = data.timePress;
      data.xQuatInitial = motionData.yQuat;
      console.log("Initial Quat");
      console.log(motionData);
    }
    else if (data.timePressInitial != 0 && compare > 45 ){
      keepAlive(wandIds);
      data.timePressInitial = data.timePress;
    }
  } else {
    data.timeRelease = Date.now();  //On first update (unpressed), save time.
    data.xQuatFinal = motionData.yQuat
    console.log("Final Quat");
    console.log(motionData);
  }

  //Button cannot be held for negative time.
  if (data.timeRelease - data.timePress > 0) {
    buttonSeconds = (data.timeRelease - data.timePress) / 1000;
  }

  //Single click behavior.
  if (buttonSeconds <= data.timeout) {
    data.buttonVal = 1; //the value of a short click.
    data.buttonPress = "Short"
    buttonSeconds = 0;

  }

  //Long click behavior.
  else if (buttonSeconds > data.timeout && buttonSeconds < 3) {
    let movementDelta = data.xQuatInitial - data.xQuatFinal;
    console.log(movementDelta);
    if (movementDelta > 50){
      data.spellColor = "Red";
    }
    else if (movementDelta < -50){
      data.spellColor = "Blue";
    }
    else {
      data.spellColor = "Yellow";
    }
    buttonSeconds = 0;
    data.xQuatInitial = 0;
    data.xQuatFinal = 0;


  
  
  }

  //Extra-long click behavior (reset to default status.)
  else {
    resetQuaternions(wandIds);
    data.buttonVal = 2; //the value of a longer click.
    data.buttonPress = "Long"
    buttonSeconds = 0;
  
  }
  return data;
}
