//++++++++++++++++++++++++++++++++++++++++++++++++++++
//balloonPath class 
//Author: Duncan Fox
//Contributors: Ryan Pfanner
//This will hold balloon pathing attributes, X, Y, Z, Speed.
//++++++++++++++++++++++++++++++++++++++++++++++++++++
export class BalloonPath{
    initialX: number = 0;
    initialY: number = 0;
    initialZ: number = 0;

    isSpawned = true;
    
    speed: number = 0;
    balloonId: number = 0;
    balloonColor: string = "Yellow";

    yellowHits: number = 0;
    blueHits: number = 0;
    redHits: number = 0;
    totalPoints: number = 0;
    totalHits: number = 0;

}

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//Balloons Class
//Author: Duncan Fox
//This class should hold an array of BalloonPath values.
//This will be necessary for props to handle the lots of balloons.
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
export class Balloon{
    // Balloons: balloonPath;
}
