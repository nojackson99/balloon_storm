//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//Game_Canvas.tsx
//Author: Duncan Fox
//Contributors: Ryan Pfanner
//Main gameplay page to demonstrate wand movement, button presses, point system, etc..
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

import { IonContent, IonHeader, IonPage, IonTitle, IonToolbar, 
  IonInput, IonItem, IonLabel, IonText, IonButton, IonCard, 
  IonCardContent, IonList, IonApp, IonMenu, IonRouterOutlet, 
  IonIcon, IonButtons, IonMenuButton, IonLoading} from '@ionic/react';
import { WandAccelData, WandButtonData } from "./BleInterface";
import { BalloonPath } from './Balloon_Objects';
import React, { useRef, useState, useLayoutEffect } from "react";
import { Canvas, useFrame } from "@react-three/fiber";
import { getCanvasSize, CanvasData } from "./Ble_Canvas_Support";
import Wand_Reticle from "./Wand_Reticle";
import { PropHasSubset } from "./three.js-master/editor/js/libs/ternjs/infer";
import Basic_Balloon from './Basic_Balloon';
import { accelAdjust } from "./Ble_Canvas_Support";
import { balloon } from "ionicons/icons";
import { isCompositeComponentWithType } from "react-dom/test-utils";

interface GameProps {
  wandAccelData: WandAccelData;
  wandButtonData: WandButtonData;
  canvasData: CanvasData;

  balloon: BalloonPath;

  onHitBoxCallback:(Balloon:BalloonPath) => void;

  onCanvasStateChanged: (data: CanvasData) => void;
}

const Game_Canvas: React.FC<GameProps> = (props) => {

  // Declaration of primary Balloon
  let balloonTest = props.balloon;
  console.log(balloonTest.isSpawned);

  //*************************
  // Despawn coordinates causes popped ballooons to respawn offscreen - UNUSED CODE
  let despawnArea = new BalloonPath();
  despawnArea.initialX = 50;
  despawnArea.initialY = 50;
  //*************************

  // RETICLE HITBOX
  let hitBox = accelAdjust(props.wandAccelData,props.canvasData);
  console.log(hitBox);
  console.log(balloonTest);
  // RETICLE  HITBOX

  // These IF statements help determine wether the wand reticle and the balloon hitboxes cross paths.
  // Depending on the color of the balloon, a certain amount of points are added to member variables of the 
  // BalloonPath class originating from Balloon_Objects.tsx -> Basic_Balloon.tsx -> Game_Canvas.tsx.
  if (balloonTest.isSpawned == true && balloonTest.balloonColor == props.wandButtonData.spellColor){
    if ((hitBox.xQuat == balloonTest.initialX || 
      hitBox.yQuat == balloonTest.initialY)) {
        balloonTest.initialX = 50;
        balloonTest.isSpawned = false;
          if(balloonTest.balloonColor == "Yellow"){
            balloonTest.totalPoints = balloonTest.totalPoints + 100;
            balloonTest.yellowHits++;
          }
          else if (balloonTest.balloonColor == "Blue"){
            balloonTest.totalPoints = balloonTest.totalPoints + 500;
            balloonTest.blueHits++;
          }
          else if (balloonTest.balloonColor == "Red"){
            balloonTest.totalPoints = balloonTest.totalPoints + 1000;
            balloonTest.redHits++;
          }
        balloonTest.totalHits++;
        console.log(balloonTest.totalPoints);
        console.log(balloonTest.totalHits)
        console.log("Position Changed and/or Hit")
        console.log(balloonTest.isSpawned);
        props.onHitBoxCallback(balloonTest);
    }
  }

  // Random Coordinate Spawn Generation for Balloons
  // Random generation for color of newly spawned balloon.
  else if (balloonTest.isSpawned == false){
    let randX = Math.floor((Math.random() * 100)) / 100;
    let randY =  Math.floor((Math.random() * 100)) / 100;
    let randInvert = Math.floor((Math.random() * 10) % 2);
    let randColor = Math.floor((Math.random() * 10) % 3);

    let doSpawn = Math.floor(Math.random() * 100) % 25;

    if (randInvert == 0){
      randX = randX * -1;
    }

    randInvert = Math.floor((Math.random() * 10) % 2);
    if (randInvert == 0){
      randY = randY * -1;
    }

    switch (randColor){
      case 0: balloonTest.balloonColor = "Yellow";
      break;
      case 1: balloonTest.balloonColor = "Blue";
      break;
      case 2: balloonTest.balloonColor = "Red";
    }
    console.log(randColor);
    console.log(balloonTest);
    
    if (doSpawn == 0){
    balloonTest.initialX = randX;
    balloonTest.initialY = randY;

    balloonTest.isSpawned = true;
    }
    props.onHitBoxCallback(balloonTest);

  }

  return (
    <IonPage>
      <IonToolbar>
          {/* Dispalying Live updates of Player gained points */}
          <IonItem slot="end">
            {"Total Points: " }{balloonTest.totalPoints}
          </IonItem>
          <IonItem slot="end">
          { "Total Hits: " }{balloonTest.totalHits}
          </IonItem>
          <IonItem slot="end">
          { "Total Yellow Hits: " }{balloonTest.yellowHits}

          </IonItem>
          <IonItem slot="end">
          { "Total Blue Hits: " }{balloonTest.blueHits}

          </IonItem>
          <IonItem slot="end">
          { "Total Red Hits: " }{balloonTest.redHits}
          </IonItem>
      </IonToolbar>
      {/* Canvas Reference into Return */}
      <Canvas
      ref={(el) => {
        if (props.canvasData.doCheck == true) {
          if (!el) return;
          getCanvasSize(props.canvasData, el); 
          console.log(props.canvasData);
        }
        props.onCanvasStateChanged(props.canvasData);
      }}
      >
      {/* Wand Reticle Component Render Code} */}
      <Wand_Reticle wandAccelData={props.wandAccelData} wandButtonData= {props.wandButtonData} canvasData={props.canvasData} />
      {/* Balloon Component Render Code  */}
      <Basic_Balloon balloonPath={props.balloon} />
      
      {/* Light composition on rendered objects */}
      <ambientLight intensity={0.1} />
      <directionalLight position={[50, 50, 50]} />
     
      </Canvas>
    </IonPage>
  );
};
export default Game_Canvas;
