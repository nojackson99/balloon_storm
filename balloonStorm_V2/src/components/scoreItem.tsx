import {
    IonItem,
    IonItemOption,
    IonItemOptions,
    IonItemSliding,
    IonLabel,
    useIonAlert,
  } from '@ionic/react';
  import { useRef, useState } from 'react';
import internal from 'stream';
  import useScoreMutations from '../graphql/useHighScoresMutations';
  import { useRealmApp } from '../Realm';
  export function ScoreItem(score: {
      playerName: string;
      gameScore: number;
      __typename: string;
      _id: string;
  }) {
      const app = useRealmApp();
      const [project] = useState(app.currentUser?.customData);
      const { updateScore } = useScoreMutations(project);
      const slidingRef = useRef<HTMLIonItemSlidingElement | null>(null);
      const [presentAlert] = useIonAlert();

      return (
          <IonItemSliding ref={slidingRef}>
              <IonItem>
                  <IonLabel>{score.playerName}</IonLabel>
                  {score.gameScore}
              </IonItem>
          </IonItemSliding>
      )
  }