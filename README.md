# CSSE4770-FALL21-G01

------------------------------------------------------------------------------------------------------------------

## Name

Balloon Storm! Run!

## Description

Balloon Storm! Run! is an augmented reality Android mobile and web application game developed using the Ionic framework with the react javascript library. The game spawns a balloon that is targeted by the user's wand reticle depending on the balloon's specified color. The player uses particular button and wand motions in order to "pop" the balloon onscreen in order to gain points within a time limit.The wand used is the sleek, popular Kano Harry Potter Bluetooth Wand.

## Motivation
- This project was developed for the learning experience of five Computer Science students for their capstone course. A customer wished for there to be an augmented reality game that enacts the use of a Kano Harry Potter wand that uses bluetooth low-energy. The backstory on the inspiration comes from the customer of a sci-fi related game that includes destroying incoming, moving, or color changing objects in front fo the player. Augmented Reality is an up-and-comer with virtual reality becoming more popular in the recent decade. These two mediums are gaining traction and this application was purposed to include aspects of such technology.

## How to play the game
<p>
Currently the project can only be deployed by pulling down the project repository, navigating to the balloonStorm_V2 or, for Android, the Android executable folder in terminal (visual stuido code, git bash, powershell, etc) and running the "ionic serve" command. This should run and display the applciation on your desired device. The applciation is hosted in your local host webpage on the users default internet browser or on your Android mobile phone. Currently, in order to play on Android, a user will need to install Android Studio.
<p>

## Installation
[Check USER_DOCUMENTATION.md]

## Development Tools used in this game
- Ionic Framework
- Capacitor
- React in Ionic (React Javascript Library)
- MongoDB Realm
- Capacitor Ble Library
- Three.js
- React Three Fiber
- React Three Fiber/drei
- React Three Fiber/xr
- Android Studio
- Kano 1007 Harry Potter Coding Kit
- Magicoo VR Headset - Adjustable Virtual Reality Goggles - Color White

## Resources / API Reference
- Ionic homepage: https://ionicframework.com/
- Ionic react docs: https://ionicframework.com/docs/react
- Capacitor Ble Library https://github.com/capacitor-community/bluetooth-le
- Three.js docs: https://threejs.org/docs/index.html
- React Three Fiber docs: https://docs.pmnd.rs/react-three-fiber/getting-started/introduction
- Harry Potter Coding Kit: https://kano.me/us/store/products/coding-wand

#### Demos we expect to show throughout the semester

#### Demo #2
- Have the app working on a mobile device.
- App has a working menu.
- Wand position is properly tracked by the app and basic menu selections can be done.


#### Demo #4 (re-evaluated)
- Application pages are routed between each other throughout the game or are all on the same page in order for basic menu selections and functions to be made.
- Have a central Main Menu with list of game options such as “Start”, “Change Name”, “Leaderboard”, “Pair Your Wand”, “Settings”, “Go Test BLE” and “Quit”.
- Wand button causes a change on screen.
- Wand motions are registered on application.
- Have balloons or blue ovals render onscreen.
- A simple leaderboard page can be displayed.

#### Demo #6 (re-evaluated)
- Render an object onscreen
- When the player clicks the "Play" button, the play-space will show.
- A leaderboard menu can be displayed.
- Different button presses will cause different changes onscreen.
- Have a central Main Menu with list of game options such as "Play", “Change Name”, “Leaderboard”, “Pair Your Wand”, “Settings”, and “Quit”.


#### Demo #8 (re-evaluated)
- The project included Harry Potter Kano wand can be paired and connected to perform wand movements.
- Harry Potter Kano wand is able to make movement of the rendered AR object in all 3 planes and including a “roll-state” to demonstrate future implementations of wand spells to destroy balloons.
- When the user presses and holds on the wand’s button, it causes the AR object to change color depending on the duration of the press.
- Holding down the kano wand button for less than 2 seconds zeros out the AR object to its starting position.
- When the player clicks the “Play” button, the play-space will show.
- When the player clicks the “Quit” button, the play-space disappears.
- A simple leaderboard menu can be displayed.
- Have a central Main Menu with list of game options such as “Play”, “Settings”, “Leaderboard”, “Pair Your Wand”, “Go Test Ble”, “Settings”, “Quit”.


#### Demo #10
- Game includes final polishing of target •	Balloon Storm working on android one plus 7 pro
- Camera working in Balloon Storm using android phone
- Balloon Storm working on android emulator through Android studio
- Basic animation of 3d objects that will be used later for balloons
- Reticle shown in app controlled by kano wand
- Harry Potter Kano wand is able to make movement of the rendered AR object in all 3 planes and including a “roll-state” to demonstrate future implementations of wand spells to destroy balloons.
- When the player clicks the “Play” button, the play-space will show.
- When the player clicks the “Quit” button, the play-space disappears.

#### Final Demo
- Application is a full web and mobile game that can be played on an Android mobile device. Specifically, a One Plus 7 Pro for the demonstration. 
- Login/Registration Page in order to play the game is displayed that's connected to MongoDB Realm Database
- A slideout menu with selections to connect your bluetooth wand, access settings, change player name, and play the game and quit teh game.
- A semi-developed augmented reality play space that includes a fully 3-Dimensional sphere and pyramid reticle, both with detectable hitboxes and color changes for immersiveness. 
- Wand movements and button presses will need to be done in order to pop balloons in the game.
- A Timer is displayed letting the player know how much time has passed in game and how much time they have remaining.
- Game can be reset by pressing the "Quit" button followed by the "Start" button.
- Permission settings can be accessed from the slideout menu.
- A point system indicating point value on the balloon color when popped by the wand reticle.
- A fully enveloped Kano Harry Potter wand is able to be connected throughout gameplay using bluetooth low-energy.

#### 

## Contributors/Authors
- Noah Jackson
- Ryan Pfanner
- Duncan Fox
- Aiden Smith
- Joseph Ciriello


## Project Status
Project was devloped by BGSU Computer Science students for a capstone course. Development concluded in December of 2021. 

## License
No current license.