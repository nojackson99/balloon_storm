---

# Any Future Misc Testing

| User Story                               | Tested Yes/No     | Did it Work?  |
|------------------------------------------|-------------------|---------------|
| [To be filled]                           | Yes               | No            |
| [To be filled]                           | na                | na            |
| [To be filled]                           | na                | na            |
| [To be filled]                           | na                | na            |
| [To be filled]                           | na                | na            |

---

# Sprint 12 Testing

| User Story                                                                                      | Tested Yes/No     | Did it Work?   |
|-------------------------------------------------------------------------------------------------|-------------------|----------------|
| Tie balloons to random function generator for colors, tied with wand motions and button presses | Yes               | Yes            |
| Mesh Creation for Reticle and Balloon Colors                                                    | Yes               | Yes            |
| Game Functionality & Point System                                                               | Yes               | Yes            |
| Implement Scoring System                                                                        | Yes               | Yes            |
| Hitboxes on Balloon Objects and Reticle                                                         | Yes               | Yes            |
| Despawn Balloon Objects                                                                         | Yes               | Yes            |
| Implement Timer on Taskbar                                                                      | Yes               | Yes            |
| Connect wand to app using community ble library                                                 | Yes               | Yes            |


--- 

# Sprint 11 Testing

| User Story                                           | Tested Yes/No      | Did it Work?  |
|------------------------------------------------------|--------------------|---------------|
| AR WebXR Permissions with Android                    | Yes                | No            |
| Finish implementing bluetooth permission             | Yes                | No            |
| Balloon Reticle and Miscellaneous Bluetooth Features | Yes                | Yes           |
| Balloon Object Animation                             | No                 | No            |
| mongoDB Realm                                        | Yes                | No            |

---

# Sprint 10 Testing

| User Story                       | Tested Yes/No      | Did it Work?  |
|----------------------------------|--------------------|---------------|
| Obeject Animation (Aiden)        | Yes                | No            |
| Obeject Animation (Duncan)       | Yes                | Yes           |
| Deploy Balloon Storm on Android  | Yes                | Yes           |
| Android with AR Live Camera Feed | Yes                | No            |
| [To be filled] (Joey)            | na                 | na            |

---

# Sprint 9 Testing

| User Story                           | Tested Yes/No      | Did it Work?  |
|--------------------------------------|--------------------|---------------|
| Android Development                  | Yes                | Yes           |
| AR Live Camera Feed                  | Yes                | No            |
| CRUD operations with Realm and Atlas | Yes                | Yes           |
| MongoDB connection and data storage  | Yes                | No            |
| Object Level Animation               | Yes                | Yes           |
| Wand pointer reticle                 | No                 | No            |
| GLTF Loading                         | Yes                | No            |

---

# Sprint 8 Testing

| User Story                             | Tested Yes/No      | Did it Work?  |
|----------------------------------------|--------------------|---------------|
| Bluetooth graphical diagnostic display | Yes                | Yes           |
| Webcam Integration with AR Objects     | Yes                | No            |
| Object Animation                       | Yes                | Yes           |
| Database                               | Yes                | No            |
| Heroku MongoDB Integration             | Yes                | No            |

---

# Sprint 7 Testing

| User Story                        | Tested Yes/No      | Did it Work?    |
|-----------------------------------|--------------------|-----------------|
| Accelerometer reset functionality | Yes                | Yes             |
| Bluetooth Diagnostic Display      | Yes                | Yes             |
| React-Three-Fiber AR              | Yes                | Yes             |
| AR object Complexity              | Yes                | No              |
| MongoDB connection                | Yes                | No              |
| MongoDB                           | Yes                | No              |


---

# Sprint 6 Testing

| User Story                                                  | Tested Yes/No      | Did it Work?  |
|-------------------------------------------------------------|--------------------|---------------|
| Implement Keep-Alive Functionality for wand                 | Yes                | Yes           |
| Implement Button Timer                                      | Yes                | Yes           |
| AR three.js integrated with typescript                      | Yes                | Yes           |
| Three.js and webxr object render                            | Yes                | Yes           |
| Assist in AR three.js and WebXR Object Rendition            | Yes                | Yes           |
| Object Creation                                             | Yes                | Yes           |
| mongoDB                                                     | Yes                | No            |

---

# Sprint 5 Testing

| User Story                                     | Tested Yes/No     | Did it Work?  |
|------------------------------------------------|-------------------|---------------|
| Bluetooth Kano Wand                            | Yes               | Yes           |
| Add Hiroku integration for mongoDB Atlas       | Yes               | Not quite     |
| AR Space                                       | Yes               | No            |
| General File Instantiation Render              | Yes               | Yes, somewhat |
| Object render three.js                         | No                | na            |

---

# Sprint 4 Testing

| User Story                                     | Tested Yes/No/na  |
|------------------------------------------------|-------------------|
| Combine code components together into the menu | Yes               |
| Bluetooth Wand                                 | Yes               |
| Combine database leaderboard                   | Yes               |
| GLTF balloon models                            | Yes               |
| Camera Page                                    | na                |

---

# Sprint 3 Testing

| User Story                                                          | Tested Yes/No/na |
|---------------------------------------------------------------------|------------------|
| Side Slide main menu with basic buttons displayed                   | Yes              |
| Leaderboard/MongoDB Realm                                           | Yes              |
| Bluetooth: Implement collaboration with wand to kano wand movements | na               |
| Fix camera showing on every page                                    | na               |
| GLTF balloon models                                                 | na               |
| GLTF files                                                          | na               |

---

# Sprint 2 Testing

| User Story                                  | Tested Yes/No/na |
|---------------------------------------------|------------------|
| Save player name                            | Yes              |
| Bluetooth service recognition               | Yes              |
| Bluetooth pairing                           | Yes              |
| Camera functionality with react             | Yes              |
| Assist with camera functionality with react | Yes              |
| Object rendering                            | Yes              |
| Location based camera view with shape       | na               |
| Plane detection based ar demo               | na               |

---

Ryan's Testing Sprint_01:

PS C:\Users\Eruption 2.0\Desktop\team1-project-three\csse4770-fall21-g01\balloonStorm> ionic serve
> react-scripts.cmd start
[react-scripts] i ｢wds｣: Project is running at http://192.168.1.9/
[react-scripts] i ｢wds｣: webpack output is served from 
[react-scripts] i ｢wds｣: 404s will fallback to /
[react-scripts] Starting the development server...
[react-scripts]
[react-scripts] You can now view balloonStorm in the browser.
[react-scripts]   Local:            http://localhost:8100
[react-scripts]   On Your Network:  http://192.168.1.9:8100
[react-scripts] Note that the development build is not optimized.
[react-scripts] To create a production build, use npm run build.

[INFO] Development server running!


  (use "git restore <file>..." to discard changes in working directory)
        modified:   src/components/ExploreContainer.css
        modified:   src/components/ExploreContainer.tsx

no changes added to commit (use "git add" and/or "git commit -a")


Ryan's Notes on TESTING.md Sprint_01: I added a cone class to the ExplorerContainer.css file to help with the skeleton code once we begin creating the augmented reality space 
and once we get it workiong with teh camera. My output from my terminal on the testing did pass and go through, however, the code is still minimal in its actions as there will
be more to add to it.

-----

Joseph Ciriello Testing: Sprint 01

Visual Code Terminal
PS C:\CS4770-g01\csse4770-fall21-g01\balloonStorm> ionic serve
> react-scripts.cmd start
[react-scripts] i ｢wds｣: Project is running at http://172.22.118.189/
[react-scripts] i ｢wds｣: webpack output is served from 
[react-scripts] i ｢wds｣: Content not from webpack is served from C:\CS4770-g01\csse4770-fall21-g01\balloonStorm\public
[react-scripts] i ｢wds｣: 404s will fallback to /
[react-scripts] Starting the development server...
[react-scripts]
[react-scripts] You can now view wandShooter in the browser.
[react-scripts]   Local:            http://localhost:8100
[react-scripts]   On Your Network:  http://172.22.118.189:8100
[react-scripts] Note that the development build is not optimized.
[react-scripts] To create a production build, use npm run build.

[INFO] Development server running!

       Local: http://localhost:8100

       Use Ctrl+C to quit this process

[INFO] Browser window opened to http://localhost:8100!

*End of terminal*

ionic serve opens web browser with text field and submit button, prompting for player name.
Clicking submit will change the page to a main menu with buttons [Play, Leadboard, Settings,
Change Name, Exit App,]
Clicking change name will change the page back to the first page.
