import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'com.balloonstorm.app',
  appName: 'Balloon Storm! Run!',
  webDir: 'www',
  bundledWebRuntime: false
};

export default config;
