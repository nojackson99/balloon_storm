import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'io.ionic.starter',
  appName: 'balloonStorm_V2',
  webDir: 'build',
  bundledWebRuntime: false
};

export default config;
