//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//Wand_Reticle Component
//Author: Duncan Fox
//This component will render a wand reticle object.  This reticle will
//Track the wand's movement on the screen and give a basis for interactions.
//
//
//
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

import { WandAccelData, WandButtonData } from "./BleInterface";
import React, { useRef, useState, useLayoutEffect } from "react";
//import * as THREE from './three.js-master/build/three';
import { Canvas, useFrame } from "@react-three/fiber";
import { OrbitControls, PerspectiveCamera } from "@react-three/drei";
import { CanvasData, accelAdjust } from "./Ble_Canvas_Support";
import { Geometry } from "./three.js-master/examples/jsm/deprecated/Geometry";

interface ReticleProps{
    wandAccelData: WandAccelData;
    wandButtonData: WandButtonData;

    canvasData: CanvasData;

}

const Wand_Reticle: React.FC<ReticleProps> = (props) => {

    let wandAccelAdjust = accelAdjust(props.wandAccelData, props.canvasData);


  return (
      <mesh position={[wandAccelAdjust.xQuat, wandAccelAdjust.yQuat, 0]} rotation={[0, 0, 0]}>
        {/* <sphereBufferGeometry attach="geometry" args={[0.5, 16, 16]} /> */}
        <coneBufferGeometry attach="geometry" args={[0.25, 0.5, 4]} />
        
        <meshStandardMaterial color={props.wandButtonData.spellColor} />
      </mesh>
  );
};
export default Wand_Reticle;
