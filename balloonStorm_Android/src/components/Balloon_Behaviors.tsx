import {BalloonPath} from './Balloon_Objects'

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//Bearing()
//Author:Duncan Fox
//This function will take a balloonPath object and modify it to produce a relative
//heading based on the balloon's initial position.
//
//)+++++++++++++++++++++++++++++++++++++++++++++++++++++++++
export function Bearing(path: BalloonPath){
    
}

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//Jitter()
//Author: Duncan Fox
//We're going to introduce some jitter 
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
export function Jitter(path: BalloonPath, difficulty: number){


}

//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//generatePath
//Author:Duncan Fox
//This function will generate the path that the balloon will follow.
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
export function generatePath(path: BalloonPath, difficulty: number){
    let adjPath = path;
    
    Bearing(adjPath);
    Jitter(adjPath, difficulty);

    return adjPath;

}


