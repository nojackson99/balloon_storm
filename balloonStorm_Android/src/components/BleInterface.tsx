 //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 //BLE Interface Classes
 //Author: Duncan Fox
 //This file contains classes for BLE operations, including UUIDs for 
 //wand services, as well as classes that will contain data pulled
 //from the different characteristics.
 //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


 //This class holds wand IDs including device ID for later use.
 export class WandIds{
    IoService = "64a70012-f691-4b93-a6f4-0968f5b648f8";
    userButton = "64a7000d-f691-4b93-a6f4-0968f5b648f8";
    ioKeepAlive = "64a7000f-f691-4b93-a6f4-0968f5B648f8";
  
    SensorService = "64a70011-f691-4b93-a6f4-0968f5b648f8";
    Quaternions = "64a70002-f691-4b93-a6f4-0968f5b648f8";
    sensorRaw = "64A7000A-F691-4B93-A6F4-0968F5B648F8";
    QuatReset = "64a70004-f691-4b93-a6f4-0968f5b648f8";

    deviceID = "0";
 }
 
 //Holds Accelerometer data from the wand.
 //Initializes all values to zero.
 export class WandAccelData {
    xQuat: number = 0;
    yQuat: number = 0;
    zQuat: number = 0;
    wQuat: number = 0;
  }

//Holds Button data gathered from the wand.
//Right now it's just the number of seconds the button's been pressed,
//But it'll likely have more stuff.
export class WandButtonData{
    timePress: number = 0;  //Keeps time in ms when button is pressed.
    timeRelease: number = 0; //Keeps time in ms when button is released.
    timePressInitial: number = 0;
    buttonStatus: number = 0;
    timeout: number = 0.2;  //Time delay in seconds to differentiate short click
    xQuatInitial: number = 0;
    xQuatFinal: number = 0;

    buttonVal: number = 0;
    buttonPress: string = "Extra Long";
    spellColor: string = "Yellow";
    
  }  


//interface to handle Callbacks.  One function for accelerometer data,
//one for Button Data.
export interface WandProps{
    onAccelStateChanged: (data: WandAccelData) => void;
    onButtonStateChanged: (data: WandButtonData) => void;
    
    wandAccelData: WandAccelData;
    wandButtonData: WandButtonData;
  }


  