//++++++++++++++++++++++++++++++++++++++++++++++++++++
//ScoreSystem Class
//Author: Ryan Pfanner
//This class will hold the game scoring points.
//++++++++++++++++++++++++++++++++++++++++++++++++++++

export class ScoreSystem{
    yellowPoint: number = 100;
    bluePoint: number = 500;
    RedPoint: number = 1000;
    totalPoints: number = 0;
    
}