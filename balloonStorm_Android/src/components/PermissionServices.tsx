import { Capacitor } from '@capacitor/core';
import { AndroidPermissions } from '@ionic-native/android-permissions';
import { BleClient } from "@capacitor-community/bluetooth-le";

const permissions = 
{
    //currently checks if BLUETOOTH_SCAN permission has been granted
    //will be changed later
    //Not being used ignore
    requestBtPermissions() 
    {
        const btPermissions = ['BLUETOOTH', 'BLUETOOTH_ADMIN', 'BLUETOOTH_CONNECT', 'BLUETOOTH_SCAN'];

        if (Capacitor.isNativePlatform())
        {
            console.log(btPermissions);

            AndroidPermissions.requestPermissions([AndroidPermissions.PERMISSION.BLUETOOTH, 
                                                  AndroidPermissions.PERMISSION.BLUETOOTH_ADMIN, 
                                                  AndroidPermissions.PERMISSION.BLUETOOTH_CONNECT, 
                                                  AndroidPermissions.PERMISSION.BLUETOOTH_SCAN])
        }
        else
        {
            console.log("Capacitor not detected. This button will do nothing :(")
            console.log(btPermissions);
        }
    },

    test_button() 
    {
        console.log("test_button called!")
    },

    //Working
    requestCameraPermission()
    {
        if(Capacitor.isNativePlatform())
        {
            AndroidPermissions.checkPermission(AndroidPermissions.PERMISSION.CAMERA).then(
                result => 
                {
                    if (result.hasPermission) 
                    {
                        //alert('you already have this permission')
                        alert('camera permission already granted');
                    } else 
                    {
                        //alert('please implement permission request')
                        AndroidPermissions.requestPermission(AndroidPermissions.PERMISSION.CAMERA);
                    }
                }
            )
        }
        else
        {
            console.log('Capacitor not detected, this button will do nothing :(')
        }
    },
    
    //Working
    requestFineLocation()
    {
        if(Capacitor.isNativePlatform())
        {
            AndroidPermissions.checkPermission(AndroidPermissions.PERMISSION.ACCESS_FINE_LOCATION).then(
                result => 
                {
                    if (result.hasPermission) 
                    {
                        alert('fine location permission already granted');
                    } else 
                    {
                        alert('fine location permission not granted');
                        AndroidPermissions.requestPermission(AndroidPermissions.PERMISSION.ACCESS_FINE_LOCATION);
                    }
                }
            )
        }
        else
        {
            console.log('Capacitor not detected, this button will do nothing :(')
        }
    },
    
}

export default permissions;