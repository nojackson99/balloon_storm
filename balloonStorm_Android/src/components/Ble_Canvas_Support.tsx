//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//Ble_Canvas_Support
//This file will contain support functions for the BLE canvas stuff.
//
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

import {WandButtonData, WandAccelData} from './BleInterface'


//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//CanvasData Class
//Author: Duncan Fox
//We're brute-forcing a way to keep track of canvas data.
//It's a surprise tool that will help us later.
//
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
export class CanvasData{
  canvasWidth: number = 0;
  canvasHeight: number = 0;

  doCheck: boolean = true;
  hasChecked: number = 0;
}

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//diagnosticColor
//Author: Duncan Fox
//diagnosticColor() takes a WandButtonData object to change the color of a generated
//cube.
//Primarily to verify button working stuff.
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
export function diagnosticColor(data: WandButtonData){
    let color = "blue";
    
    if (data.buttonStatus == 1) {
        color = "yellow";
      } else if (data.buttonStatus != 1) {
        if (data.buttonVal == 1) {
          color = "green";
        } else if (data.buttonVal == 2) {
          color = "red";
        } else if (data.buttonVal == 0) {
          color = "blue";
        }
      }
      return color;
}



//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//getCanvasSize()
//Author: Duncan Fox
//this function will take CanvasData and HTMLCanvasElement objects and strip the height and width
//for use in the accelAdjust function.
//
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

export function getCanvasSize(data: CanvasData, el: HTMLCanvasElement){
  data.canvasWidth = el.getBoundingClientRect().width
  data.canvasHeight = el.getBoundingClientRect().height
  data.hasChecked ++;

  if (data.hasChecked > 4){
    data.doCheck = false; //this should shut down the canvas checker.
  }

}

//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//accelAdjust()
//Author: Duncan Fox
//This program will take WandAccelData and CanvasData objects and use them to provide
//adjusted values for object positioning.  It will then return a new wandAccelData object.
//
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

export function accelAdjust(accelData: WandAccelData, canvasData: CanvasData){
let adjAccel = new WandAccelData();

let adjWidth = canvasData.canvasWidth / 2;
let adjHeight = canvasData.canvasHeight / 2;

adjAccel.xQuat = -1 * (accelData.xQuat / 100);
adjAccel.yQuat = (accelData.yQuat / 100);

return adjAccel;


}