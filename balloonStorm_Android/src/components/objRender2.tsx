// Author: Ryan Pfanner
// co-Author(s): Aiden Smith
//
// objrender.tsx DOCUMENTATION (Below)
/*
  Notes: 
*/
//
// MAIN SECTION (Aiden Smith, Ryan Pfanner)
import React, {useRef, useState} from "react";
import * as THREE from './three.js-master/build/three';
// RENDER SECTION (Aiden Smith)
import { Canvas, useFrame } from "@react-three/fiber";
import { OrbitControls, PerspectiveCamera } from "@react-three/drei";
// AR SECTION (Ryan Pfanner)
import { ARCanvas, Interactive, DefaultXRControllers } from '@react-three/xr'
//
//const xr = navigator.xr;
import ReactDOM from 'react-dom'
//const { controllers } = useXR()

function Box({ color, size, scale, children, ...rest }: any) {
  return (
    <mesh scale={scale} {...rest}>
      <boxBufferGeometry attach="geometry" args={size} />
      <meshPhongMaterial attach="material" color={color} />
      {children} 
    </mesh>
  )
}

function Button(props: any) {
  const [hover, setHover] = useState(false)
  const [color, setColor] = useState<any>('blue')

  const onSelect = () => {
    setColor((Math.random() * 0xffffff) | 0)
  }

  return (
    <Interactive onHover={() => setHover(true)} onBlur={() => setHover(false)} onSelect={onSelect}>
      <Box color={color} scale={hover ? [0.6, 0.6, 0.6] : [0.5, 0.5, 0.5]} size={[0.4, 0.1, 0.1]} {...props}>
        
      </Box>
    </Interactive>
  )
}

function App2() {
  
  return (
    <ARCanvas>
      <ambientLight />
      <pointLight position={[10, 10, 10]} />
      <Button position={[0, 0.5, -0.5]} />
      
    </ARCanvas>
  )
}

ReactDOM.render(<App2 />, document.getElementById('root'))
/*
INSERT BETWEEN boxBuffGeometry and meshStandardMaterial: 
                          <OrbitControls enablePan={true} enableZoom={true} enableRotate={true} 
                          addEventListener={undefined} hasEventListener={undefined} 
                          removeEventListener={undefined} dispatchEvent={undefined} />
*/

/*
function ObjRender2() {
  return (
    
    <ARCanvas>
        <ambientLight intensity={0.1} />
        <directionalLight color="blue" position={[50, 50, 50]} />
        <mesh>
            <boxGeometry />
            <meshStandardMaterial />
        </mesh>
    </ARCanvas>
    
  );
};
*/

/* const SpinningBox = ( ) => {

    const mesh = useRef(null);
    useFrame( () => (mesh.current.rotation.x = mesh.current.rotation.y += 0.01))
        return (
            <div>
                    <mesh ref={mesh}>
                    <boxBufferGeometry attach='geometry' args={[1,1,1]}/>
                    <meshStandardMaterial attach='material' />
                    </mesh>
            </div>
        );
}; */

export default App2
