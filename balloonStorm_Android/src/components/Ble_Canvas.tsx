//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//Ble_Canvas.tsx
//Author: Duncan Fox
//Diagnostic page to demonstrate wand movement, button presses.
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

import { WandAccelData, WandButtonData } from "./BleInterface";
import React, { useRef, useState, useLayoutEffect } from "react";
//import * as THREE from './three.js-master/build/three';
import { Canvas, useFrame } from "@react-three/fiber";
import { OrbitControls, PerspectiveCamera } from "@react-three/drei";
import { diagnosticColor, getCanvasSize, CanvasData, accelAdjust } from "./Ble_Canvas_Support";
import  Wand_Reticle  from "./Wand_Reticle";

interface DiagnosticProps {
  wandAccelData: WandAccelData;
  wandButtonData: WandButtonData;
  canvasData: CanvasData;


  onCanvasStateChanged: (data: CanvasData) => void;
}

const Ble_Canvas: React.FC<DiagnosticProps> = (props) => {
  //let zQuat = -1 * (props.wandAccelData.zQuat / 100);
  //let wQuat = props.wandAccelData.wQuat / 100;

  //let wandAccelAdjust = accelAdjust(props.wandAccelData, props.canvasData);

  let color = diagnosticColor(props.wandButtonData);
  let data = new CanvasData();


  return (
    <Canvas
      ref={(el) => {
        if (props.canvasData.doCheck == true) {
          if (!el) return;
          getCanvasSize(props.canvasData, el); //By rights, this shouldn't work, but it does.  Why?
          console.log(props.canvasData);
        }
        props.onCanvasStateChanged(props.canvasData);
      }}
    >

      <Wand_Reticle wandAccelData={props.wandAccelData} wandButtonData={props.wandButtonData} canvasData={props.canvasData}/>
      <ambientLight intensity={0.1} />
      <directionalLight color={color} position={[50, 50, 50]} />
    </Canvas>
  );
};
export default Ble_Canvas;
