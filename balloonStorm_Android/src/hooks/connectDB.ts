import { IonContent } from '@ionic/react';
/*
NOTE: Adding in the above import because: Typescript treats files without import/exports as legacy script files. 
As such files are not modules and any definitions they have get merged in the global namespace. isolatedModules forbids such files.
Adding any import or export to a file makes it a module and the error disappears.
*/

/*
import Realm from "realm";

const highScoresSchema = {
    name: "highScores",
    properties: {
        playerName: "string",
        score: "int",
        sessionStart: "date"
    }
};
*/
/*
try {
    const realm = await Realm.open({
        path: "balloonStormGameData",
        schema: [highScoresSchema],
    });
} catch (err: any) {
    console.error("Failed to open the realm", err.message);
};

export {}
*/


