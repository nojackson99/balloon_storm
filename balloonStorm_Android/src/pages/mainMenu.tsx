import { IonContent, IonHeader, IonPage, IonTitle, IonToolbar, 
    IonInput, IonItem, IonLabel, IonText, IonButton, IonCard, IonCardContent} from '@ionic/react';
import { stringify } from 'querystring';
import React, { useState, useRef } from 'react';
import './mainMenu.css';


// Initial change (Ryan)

const MainMenu: React.FC = () => {
  const [playerName, setPlayerName] = useState<string>(); 

  const nameInputRef = useRef<HTMLIonInputElement>(null);
  const setName = () => {
    const enteredName = nameInputRef.current!.value;
    if(!enteredName){
      return;
    }
    setPlayerName(JSON.stringify(enteredName));
  };
  const clearName = () => {
    setPlayerName('');
  }

    return (
        <IonPage>
          <IonContent fullscreen>
            <IonCard>
              {!playerName && (<IonCardContent>
                <div className="inputName">
                  <IonItem>
                    <IonInput ref={nameInputRef} id="userName" inputMode="text" maxlength={15} placeholder="Enter player name"></IonInput>
                  </IonItem>
                  <IonButton onClick={setName} id="submit" type="submit" size="large">Submit</IonButton>
                </div>
              </IonCardContent>)}
              {playerName && (<IonCardContent id="pageContent">
                <h1>Hello {playerName}, welcome to Balloon Storm!</h1>
                <div id="menuButtons">
                  <IonButton routerLink="/Home">Play Game</IonButton>
                  <IonButton routerLink="/leaderboard">Leaderboard</IonButton>
                  <IonButton onClick={clearName}>Change Name</IonButton>
                  <IonButton>Settings</IonButton>
                  <IonButton>Quit Game</IonButton>
                </div>
              </IonCardContent>)}
            </IonCard>
          </IonContent>
        </IonPage>
      );
};

export default MainMenu;