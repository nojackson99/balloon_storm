import { IonContent, IonHeader, IonPage, IonTitle, IonToolbar, IonButton } from '@ionic/react';

const LeaderBoard: React.FC = () => {
    return (
      <IonPage>
        <IonHeader>
          <IonToolbar>
            <IonTitle>High Scores!</IonTitle>
          </IonToolbar>
        </IonHeader>
        <IonContent fullscreen>
          <IonHeader collapse="condense">
            <IonToolbar>
              <IonTitle size="large">Blank</IonTitle>
            </IonToolbar>
          </IonHeader>
          <IonButton>Test BD Stuff</IonButton>
          <IonButton routerLink="/mainMenu">Go Back</IonButton>
        </IonContent>
      </IonPage>
    );
  };

  export default LeaderBoard;