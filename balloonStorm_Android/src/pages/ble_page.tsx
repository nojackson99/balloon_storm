//+++++++++++++++++++++++++++++++++++++++++++++++++
//ble_page.tsx
//Author: Duncan Fox
//This page contains the core BLE interpretation functions.
//Support functions are contained within Ble_SupportFunctions.tsx.
//+++++++++++++++++++++++++++++++++++++++++++++++++

import {IonButton} from "@ionic/react";
import ExploreContainer from "../components/ExploreContainer";
import {
  BleClient,
  BleDevice
} from "@capacitor-community/bluetooth-le";
import { useState } from "react";
import {WandAccelData, WandButtonData, WandIds, WandProps} from '../components/BleInterface';
import {parseTilt, parseButton} from '../components/Ble_SupportFunctions'


const Ble_Connector: React.FC<WandProps> = (props) => {
  const [buttonClass, setButtonClass] = useState("ButtonDefault");
  let wandIds :WandIds = new WandIds();
  let wandButtonData = props.wandButtonData;
  let wandAccelData = props.wandAccelData;



  //BLE_Test gets passed the result of the promise made in the request() function.
  //This provides the device ID of the wand for later connection/initiation.
  async function Ble_Interpreter(device: BleDevice) {
    await BleClient.initialize(); //Initializes Bluetooth pieces.
    await BleClient.connect(device.deviceId).catch((error: any) => {
      console.log(error);
        });
    wandIds.deviceID = device.deviceId; //Update wand's device ID.  This allows use of multiple different wands.
    console.log(wandIds.deviceID);
    console.log("connected to device", device); //Verify connection to Wand

  //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  //This function monitors the accelerometer for a change.
  //On an accelerometer state change, it will signal the parseTilt function to perform the 
  //interpretation of accelerometer data.
  //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  await BleClient.startNotifications(
    device.deviceId,
    wandIds.SensorService,
    wandIds.Quaternions,
    (value: DataView) => {
      props.onAccelStateChanged(parseTilt(value));
      cachedAccelData = parseTilt(value);
    }
  ).catch((error: any) => {
    console.log(error);
  });

  let cachedAccelData = new WandAccelData();
  //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  //This function monitors the wand button for changes.  
  //On a button state change it will signal the parseButton function to 
  //perform interpretation on the button signal.
  //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    await BleClient.startNotifications(
      device.deviceId,
      wandIds.IoService,
      wandIds.userButton,
      (value: DataView) => {
        props.onButtonStateChanged(parseButton(value, props.wandButtonData, cachedAccelData, wandIds));
      }
    ).catch((error: any) => {
      console.log(error);
    });


  }


  return (
    <div>
      <IonButton color="tertiary"
        onClick={() => {
          BleClient.initialize();
          BleClient.requestDevice({
            namePrefix: 'Kano',
            optionalServices: [wandIds.IoService, wandIds.SensorService],
          }).then((result: any) => Ble_Interpreter(result));
        }}
      >
        Pair your Wand!
      </IonButton>
    </div>
  );
};

export default Ble_Connector;
