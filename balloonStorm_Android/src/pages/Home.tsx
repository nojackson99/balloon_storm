//+++++++++++++++++++++++++++++++++++++++++++++++++++
//home.tsx
//Author: Duncan Fox
//Dummy Page to verify bluetooth functionality.
//+++++++++++++++++++++++++++++++++++++++++++++++++++

import { IonContent, IonHeader, IonPage, IonTitle, IonToolbar, IonButton } from '@ionic/react';
import BleWandDiagnosticView from '../components/BleWandDiagnosticView';
import ExploreContainer from '../components/ExploreContainer';
import Ble_Connector from './ble_page';
import {useState} from 'react';
import './Home.css';
import {WandAccelData, WandButtonData} from '../components/BleInterface';
import {CanvasData } from '../components/Ble_Canvas_Support'
import Ble_Canvas from '../components/Ble_Canvas'

const Home: React.FC = () => {
  function WandAccelCallback(data: WandAccelData){
    setWandAccelData(data);
  }
  function WandButtonCallback(data: WandButtonData){
    setWandButtonData(data);
  }
  function CanvasCallback(data: CanvasData){
    setCanvasData(data);
  }

  const [wandAccelData, setWandAccelData] = useState<WandAccelData>(
    new WandAccelData()
  );
  const [wandButtonData, setWandButtonData] = useState<WandButtonData>(new WandButtonData());
  const [canvasData, setCanvasData] = useState<CanvasData>(new CanvasData());

  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonTitle>Blank</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent fullscreen>
        <IonHeader collapse="condense">
          <IonToolbar>
            <IonTitle size="large">Blank</IonTitle>
          
          </IonToolbar>
        </IonHeader>
        <Ble_Connector onAccelStateChanged={WandAccelCallback} onButtonStateChanged={WandButtonCallback} wandAccelData={wandAccelData} wandButtonData={wandButtonData}></Ble_Connector>
        <BleWandDiagnosticView wandAccelData={wandAccelData} wandButtonData={wandButtonData}/>
        <Ble_Canvas wandAccelData={wandAccelData} wandButtonData={wandButtonData} onCanvasStateChanged={CanvasCallback} canvasData={canvasData}/>
      </IonContent>
    </IonPage>
  );
};

export default Home;
