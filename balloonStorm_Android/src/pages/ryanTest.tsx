import { IonContent, IonHeader, IonPage, IonTitle, IonToolbar, 
    IonInput, IonItem, IonLabel, IonText, IonButton, IonCard, 
    IonCardContent, IonList, IonApp, IonMenu, IonRouterOutlet, 
    IonIcon, IonButtons, IonMenuButton} from '@ionic/react';
//
import * as THREE from '../components/three.js-master/build/three.js';
    
  const RyanTest: React.FC = () => {

    return (
      <IonApp>
        <IonPage>
            <h2>ryanTest Page</h2>
            <h2>Another commit</h2>
            <h3>an initial commit</h3>
            <h4>initial commit</h4>
        </IonPage>
      </IonApp>
    );
  };

  export default RyanTest;