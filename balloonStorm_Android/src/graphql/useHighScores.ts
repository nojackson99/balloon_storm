import { useQuery } from "@apollo/client";
import gql from "graphql-tag";
import useHighScoreMutations from "./useHighScoresMutations";

const useScores = (project: any) => {
    const { scores, loading } = useAllScoresInProject(project);
    const { addScore, updateScore } = useHighScoreMutations(project);
    return {
        loading,
        scores,
        updateScore,
        addScore,
    };
};
export default useScores;

function useAllScoresInProject(project: any){
    const { data, loading, error } = useQuery(
        gql`
            query GetAllScoresForProject($id: ObjectId!) {
                highScores(query: { _id: $id }) {
                    playerName
                    gameScore
                }
            }
        `,
        {variables: {id: project.id}}
    );
    if(error){
        throw new Error(`Failed to fetch tasks: ${error.message}`);
    }
    const scores = data?.scores ?? [];
    return {scores, loading};
}