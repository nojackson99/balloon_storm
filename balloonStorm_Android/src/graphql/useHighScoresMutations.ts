import { ObjectId } from 'bson';
import { useMutation } from '@apollo/client';
import gql from 'graphql-tag';

export default function useHighScoreMutations(project: any) {
    return {
        addScore: useAddScore(project),
        updateScore: useUpdateScore(project),
    };
}

const AddScoreMutation = gql`
    mutation AddScore($score: HighScoreInsertInput!){
        addedScore: insertOneHighScore(data: $score){
            _id
            playerName
            gameScore
        }
    }
`;

const UpdateScoreMutation = gql`
  mutation UpdateScore($scoreId: ObjectId!, $updates: HighScoreUpdateInput!) {
    updatedScore: updateOneHighScore(query: { _id: $scoreId }, set: $updates) {
      _id
      playerName
      gameScore
    }
  }
`;

const ScoreFieldsFragment = gql`
    fragment HighScoreFields on HighScore {
        _id
        playerName
        gameScore
    }
`;

function useAddScore(project: any) {
    const [addScoreMutation] = useMutation(AddScoreMutation, {
      update: (cache, { data: { addedScore } }) => {
        cache.modify({
          fields: {
            tasks: (existingScores = []) => [
              ...existingScores,
              cache.writeFragment({
                data: addedScore,
                fragment: ScoreFieldsFragment,
              }),
            ],
          },
        });
      },
    });

    const addScore = async (score: any) => {
        const {
          data: { addedScore },
        } = await addScoreMutation({
          variables: {
            score: {
              _id: new ObjectId(),
              _partition: project.particion,
              ...score,
            },
          },
        });
        return addedScore;
      };
    
      return addScore;
    }

    function useUpdateScore(project: any) {
        const [updateScoreMutation] = useMutation(UpdateScoreMutation);
        const updateScore = async (score: any, updates: any) => {
          const {
            data: { updatedScore },
          } = await updateScoreMutation({
            variables: { scoreId: score._id, updates },
          });
          return updatedScore;
        };
        return updateScore;
      }