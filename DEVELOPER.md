## Project high-Level Summary:
<p>
An augmented reality game that uses bluetooth low-energy to connect to a Harry Potter Kano Wand. A OnePlus 7 Pro mobile phone will run Balloon Storm! Run!. The applciation will instigate the player to register for the game using an email and password, and after registering, said credentials will allow player to signin for the remainder of the user's account. After signing in, user input will be requested asking for the wizard or witch's name. A Toolbar menu will be seen and a slidable-out menu will be accessible. The side menu includes navigation throughout the project's features. After pairing their wand using the "Pair Your Wand" button, players will then press the "Play" button to then begin targeting the balloon objects to increase their points in an allotted amount of time.
</p>

---

## Data/Data Storage:
<p>
Data is stored using mongoDB Realm, a developmental platform designed for modern, data-driven applications. Realm is used to build mobile, web, and desktop software. 
The types of data Balloon Storm! Run! will store will include:
    1) User email address
    2) User's desired password for Balloon Storm! Run! account
    Note: On the Android mobile version, data will not be taken from the user through this means of transfer. No data will be extracted from the user on the mobile version whatsoever in the final product of this applciation. Future updates of this applciation could include the use of data extraction from user input into the game.
Data will be stored with the assistance of of MongoDB Realm, as previously stated. Future renditions of teh applciation may include data being stored in the database such as player username, player total points, total hits, and other miscellaneous statistics.
Data is extracted on the web version of Balloon Storm! Run! and is stored safely through a remote server.
<p>

---

## Design/Functionality of our UI:
<p>
There are seperate pages on our project that include...

* Login/registration Page
* Username Input Block
* A center screen displaying a top-bar menu which will include the gameplay timer in the far top-right corner.
* Slideout Menu on right side of screen.
* * Play Button: Displays timer, point stats, and 3D augmented Reality objects in ceneter of the screen.
* * Change Name Button: Allows the palyer to re-enter their username which then reapears on top of the slideout menu.
* * Leaderboard Button: May display input blocks where the player may add the results of their previous playthrough.
* * Pair Your Wand Button: Displays a drop down indicator showing all nearby bluetooth devices.
* * Go Test Ble Button: Allows the player to practice with their wand and moving around a dummy object. Starts shown are the coordinates in which their wand moves.
* * Test Leaderboard: To be removed/Alread removed feature/button.
* * Settings Button: Includes buttons to instigate requests for device permissions needed for proper app funciton on Android.
<p>

---

#### Project Contributors:
* Joseph Ciriello
* Ryan Pfanner
* Duncan Fox
* Noah Jackson
* Aiden Smith
