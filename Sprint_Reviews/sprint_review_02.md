### Noah's Answers

1. What went well?
<p>
We have the camera working to show the eventual ar space and have made progress on displaying a menu that will allow for starting the game, viewing high scores, etc. We have also made progress with getting the app to recognize and pair with our Bluetooth wand
</p>
2. What didn't go so well?
<p>
We are still trying to determine what kind of ar we want to use for showing our balloon targets, current ideas are using location based, plane based, or marker based ar.
</p>
3. What have I learned?
<p>
I have learned a lot more about how ionic works with react and exactly what react, ionic, and capacitor are and how each piece of software works together to deliver a final product.
</p>
4. What still puzzles me?
<p>
I need to do more work to learn how typescript works and how it will used in our source code. Also still working on figuring out what JavaScript framework we want to use in displaying our balloon targets. Currently we are looking at using ar.js to display this and more research will lead to a final decision.
</p>
5. What will we change to improve? 
<p>
Overall, not much needs to change in how we are dividing up work and how our workflow is progressing. We will continue our work in sprint 3 to further develop the final product.  
</p>

---

### Aiden's Answers  

1. What went well?
<p>
This week I feel a lot more confident in navigating files on the server. Before this, I wasn’t quite sure what files did what inside our server folder, but with more practice as this sprint went along, I felt myself growing more accustom to it.
</p>
2. What didn't go so well?
<p>
This week I felt like I had a lot of downtime. Ryan and Noah completed my user story on accident pretty early on in the sprint, on Sunday. They told me what they did and where I can learn more about it, but they have the code written for rendering a shape in AR. So, I spent the rest of the sprint studying arjs and a-frame, and how they work.  
</p>
3. What have I learned?
<p>
This sprint taught me a lot about arjs and the a-frame libraries. Now that I understand how to navigate around the Ionic/React framework, I feel comfortable with using a-frame and arjs to create ar shapes.  
</p>
4. What still puzzles me?
<p>
I’m still struggling with fully understanding Git and staying up to date with my teammates, but I’ve already made lots of progress in my version-control knowledge. Once I finally get the hang of git, I’ll feel a lot more comfortable on pushing, pulling, merging, etc.  
</p>
5. What will we change to improve? 
<p>
I believe our group needs to rewrite our user stories, because they aren’t quite precise enough. We plan on revisiting them on the 23rd or 24th. 
</p>

---

# Duncan Individual Sprint doc

### What went well?
I can now pair with a specific bluetooth device and access services.  The Ionic app can now recognize changes in state and push a notification to the console.

### What didn't go so well?
Merging is still a little bit dicey, but it wasn't nearly as bad as the last sprint which broke the entire app, so there's progress being made.  Notifications seem also to time out after a few minutes.  This might be why there's a 'keep alive' characteristic.

### What have I learned?
In order to connect with a bluetooth device, Ionic requires user intervention.  This seems to be a security feature for bluetooth web services -- you don't want to be accessing BLE services all willy-nilly.  Also learned about promises and asynchronous functions 

### What still puzzles me?
I'm still not sure that I totally understand arrow functions or promises.  In particular, I'd like to be able to extract the promise values to a variable that can be used later.

### What will we have to change to improve?
Communication and our understanding of the react framework will need to improve.  We attempted to do the majority of our work as self-contained units, but by accidentally editing a global file, we kind of threw everything off.

---

### Ryan’s Answers

1. What went well?
<p>
We have working software at the end here of sprint two. My personal code, I was able to render an augmented reality object as a blue sphere using a-frame. A main menu was made successfully along with Bluetooth recognition into the Harry Potter wand. 
</p>
2. What didn't go so well?
<p>
There was some trouble with integrating the pages to work together on one page but to remedy this, for now, we split them up into their own pages and plan to bring them together at some point. 
</p>
3. What have I learned?
<p>
I learned how to merge the correct way with multiple people working on one directory. With sprint one not pulling down from the main branch before merging was causing problems. Previously, I had thought that I could just merge my own branch with the main branch simply by git merge. The problem is, I wasn’t checking out the main branch and then pulling down before merging. One of my team members explained this to me. I’m learning so much with version control through trial and error. Even though there are mistakes, I’m at least able to learn from it.
</p>
4. What still puzzles me?
<p>
My second user story was working with location based AR where we wanted to invest some time in using un-marked AR so that we wouldn’t have to rely on our “hiro” markers to hold in front of the camera in order to render.
</p>
5. What will we change to improve? 
<p>
More researching can always be done to mitigate bug fixes and problem that we have. Looking deeper into Typescript (.tsx) and ionic/react as a whole will be done weekly for all team members. Since there are five of us in the group, it’s tough come up with doable user stories for that week, which makes coding them a bit more difficult, so we can spend more time discussing if our user stories are achievable for current sprints. 
</p>

---



