### Ryan’s Answers

## 1. What went well?
<p>
I have laerned a lot but when it comes to getting to my goal for this sprint, not a whole lot went my way. I'm able to render objects, however, getting it implemented into our file system is deeming challenging.
</p>
## 2. What didn't go so well?
<p>
I'm having trouble, as mentioend before, getting some javascript code to work alongside our typescript files. This is a roadblock for a couple of us who are focusing in on the augmented reality player space.
</p>
## 3. What have I learned?
<p>
I've learned wuite alot about three.js which entails our AR rendering for our project as well as some javascript along the way.
</p>
## 4. What still puzzles me?
<p>
The main problem is really just getting our javascript files/code to work with the typescript. This si the main challenge moving forward.
</p>
## 5. What will we change to improve? 
<p>
We hadn't met up this week very much, due to fall break, I think this was a deterent from us meeting as often has we would in prior sprints. Moving forward, we'll make sure we meet more often. I personally think I should communicate more often with my team members who are also working onthe AR space rendering, but again, I think it was due to our fall break messing up my mojo, but I should be communicating more often as bugs arise in our code.
</p>

---

## Duncan's Answers

### What went well?
The bluetooth wand has a diagnostic page, and I'm beginning to separate some of the parts into distinct components to use props to pass values from compoenent to component.

### What didn't go so well?
Props still aren't working the way that I'd necessarily intended.  The next step is to develop an interface class to handle the different parts of the tilt quaternion.

### What have I learned?
Classes are there to make things easier, probably.  It's never been a decision that I've had to consciously make, but I'm beginning to see the benefits of object-oriented programming.  It's probably going to be easier to pass information along within a class, especially given that I've already split the quaternions up in a parser.

### What still puzzles me?
Props.  I understand what they are and basically how they work in javascript, but not necessarily how to shift that knowledge from javascript to typescript, because even though typescript is basically "Javascript plus", it's not plug and play.

### What will we change to improve? 
We didn't meet a whole lot this week, because of the break we had.  While that's not a huge deal for those of us that are working alone on parts, it's still a good idea to sort of touch base every other day or so and make sure we all know what's going on.

---

### Aiden's Answers


## 1. What went well?
<p>I found great resources in how to create objects in blender. I've spent time learning how to use blender and how .obj files work, animations, and creating basic objects.</p>

## 2. What didn't go so well?
<p>We still are not able to get an object to render on the screen, so any blender object I make is not able to get loaded until the loader is working</p>

## 3. What have I learned?
<p>Learning how to use blender is very useful to know for the future if I do any 3D work after this class. As I mentioned in Question 1, I've been learning blender and 3D animation.

## 4. What still puzzles me?
<p>Trying to detail these renders is pretty complicated, so I might just make more basic renders, compared to less but extra detailed renders.</p>


## 5. What will we change to improve? 
<p>There is not much we can change to improve, at this point it is just a matter of getting the objects to render at this point. It's the biggest hurdle our group has had to face so far.</p>

---

### Joey's Answers
## 1. What went well?
Not very much. I did a lot of research on how to add a project to Heroku servers but have not been able to successfully push the code to their repositories.

## 2. What didn't go so well?
I could not find a buildpack for Heroku from online guides that would make the repository accept the push to main on the Heroku server.

## 3. What have I learned?
The issue was the format of the project on gitlab. The fact that the the .json files are not on the top level of the repository does not agree with pushing to Heroku.

## 4. What still puzzles me?
With the recommendation from a classmate in the walking shapes group, I am going to create another project repository in GitLab to push to the Heroku server. I am curious to see how this pans out with updating code on GitLab in the future.

## 5. What will we change to improve?
I will try to be more open to the idea of communicating with others when I am struggling to solve a problem on my own.
---

### Noah's Answers

## 1. What went well?
<p>
Found some good resources to continue investigation into how to render objects with three.js in ionic react. Hopefully I can use these to get closer to figuring out how object rendering.
</p>
## 2. What didn't go so well?
<p>
Overall less progress made than I wanted. Still frustrated with how to work with javascript libraries like three.js within typescript.
</p>
## 3. What have I learned?
<p>
I have learned more about what is needed to render a scene in three.js. This includes a camera, scene, object, light, etc.
</p>
## 4. What still puzzles me?
<p>
The syntax and lines of code needed to utilize three.js functionality within ionic react.
</p>
## 5. What will we change to improve? 
<p>
Continue to focus multiple team members on the same object rendering task, hopefully we can get some solid commits that will enable object rendering within the project.
</p>

---
