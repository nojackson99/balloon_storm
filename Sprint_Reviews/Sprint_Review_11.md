### Ryan's Answers

## 1. What went well?
<p>
I think we can still push through the rest of this sprint 11 and through 12 and have something to demo by the final presenation date. 
</p>

## 2. What didn't go so well?
<p>
I feel as though we haven't gotten as far as we wanted to when we first set out to work on this project in sprint 1. It is our first time wokring on an ambitious project such as this and it's suspected that we'll run into snags. I think we, as a group, can learn that the software development process doesn't always go as scheduled, especially as it's a fresh experience for us. It's important for my team members and I to stay with a level-headed mind going forward because it's very easy to get discouraged as I'm sure the others are discouraged as I have. This is a fact of life though and when things get difficult, its those moments that help develop our person, and our experience as software learners and future developers.
</p>

## 3. What have I learned?
<p>
I've learned how to keep a level-headed mind and keep my eyes on the future. The development process doesn't always go as scheduled, as I mentioned in my previous answer and that it's important to keep pushing and to try your best regardless.
</p>

## 4. What still puzzles me?
<p>
I'm wokring on hitboxes for our basic game functionality, and I'm learning about the game canvas characteristics and how I can add attributes to the objects and reticle that we have.
</p>

## 5. What will we change to improve? 
<p>
We'll have to keep forward and to realize that we're learning and to take this project as such, a learning opportunity. I still think we tried real well, and even thoough we've run into many snags along the way, we can still make sure we keep with it regardless of the outcome.
</p>

---
### Noah's Answers

## 1. What went well?
<p>
Android permissions are working correctly for the camera, still having trouble with bluetooth permissions and with connecting the wand to our app on android.
</p>

## 2. What didn't go so well?
<p>
Having trouble getting our project to prompt the user for certain bluetooth permissions that are needed for proper functioning, not sure why this is happening.
</p>

## 3. What have I learned?
<p>
I have learned how to the various components of the main menu are shown/hidden.
</p>

## 4. What still puzzles me?
<p>
Not sure how to connect the wand to our app. Also not sure if that's all that is needed for proper funcitoning and tracking for the want.
</p>

## 5. What will we change to improve? 
<p>
I have talked to Alex on the walking shapes team, he is working on the android version of their project and was able to give me some pointers on how to go about implementing the permissions
</p>

### Duncan's Answers

## 1. What went well?
The shift to new objectives worked out reasonably well.  Both the balloons and the wand's reticle can have different colors, and the wand reticle's color can be changed.  Once, but that's progress.

## 2. What didn't go so well?
The wand reticle is supposed to be able to change with wand motion.  The implementation is currently very simple -- take an inital point and a final point and compare.  Unfortunately, I can't yet pass the wand accelerometer data to the button parser as I had originally invisioned.

## 3. What have I learned?
Problems that seem simple can often be more complex.  Modularity in programming makes making adjustments reasonably straightforward.

## 4. What still puzzles me?
Why can't I pass the wand accelerometer data to the button parser?  I guess I'll have to figure it out in a week.

## 5. What will we change to improve? 
More meetings, maybe?  I have no real idea.  Any improvements are going to be pretty much moot, since this is coming up on the last sprint.  This project's primary value is in the learning experience, anyway.

---
### Aiden's Answers

## 1. What went well?
<p>
A balloon shape was able to be rendered this week, and the reticle that will be used to aim at the balloons is also functioning right now. Progress is coming along with rendering.
</p>

## 2. What didn't go so well?
<p>
GLTFs have turned out to be a complete bust, so we're having to switch to primative objects within RTF to represent a balloon.
</p>

## 3. What have I learned?
<p>
I've learned how to be patient with my writing, and that writing code while frustrated will commonly result in poor quality code. It's hard keeping a level-head sometimes, especially with the end in sight and so much still has to be done.
</p>

## 4. What still puzzles me?
<p>
How props are properly passed between components. I understand the problem conceptually, but I'm bad at translating that into functioning code.
</p>

## 5. What will we change to improve? 
<p>
More meetings, I suppose? We're meeting a couple times a week, but most of us are busy outside of the project. I'm meeting with Duncan about animation and working with him on that front, but with this being the final week, most of the changes are ultimately going to be unfinished. I've learned so much from this course about how big projects are actually structured, and what to expect from something like this in the future. But, the content itseld proved to be its own different challenge.
</p>

---

### Joey's Answers

## 1. What went well?
<p>
Implementing register and login functions to connect a user account to the MongoDB Realm backend associated with the balloonstorm app. This corrected the "must be logged in to use GraphQL" error, and I was then able to correct the rest of the errors so that the application runs with the GraphQL functions that I implemented last sprint.
</p>

## 2. What didn't go so well?
<p>
The function to display data stored in the database did not display the entries I submitted via the Realm UI on the mongodb website. I believe that this is because that data is likely not associated with newly registered accounts, and I will first have to make functions to insert scores first.
</p>

## 3. What have I learned?
<p>
MongoDB Realm is actually pretty cool. The existing documentation does not provide detailed solutions for how to make everything work with and Ionic React framework, but now that I've deciphered the tutorial I've been trying to follow for the past few weeks, I've come to appreciate how powerful this backend system can be. There even seem to be simple methods to implement email verification when creating new accounts through Realm, but I am not utilizing this for now.
</p>

## 4. What still puzzles me?
<p>
Why am I able to insert documents to the mongoDB Atlas collection if they are not associated with new accounts by default? Or is my assumption about why they don't display in the app incorrect?
</p>

## 5. What will we change to improve? 
<p>
Just keep trying everything, and shift into overdrive for this last week.
</p>
