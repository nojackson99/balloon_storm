### Ryan's Answers

## 1. What went well?
<p>
We made further progress on the augmented reality space and bluetooth. We had merge conflicts but they were handled diligently. 
</p>

## 2. What didn't go so well?
<p>
We're still having some trouble with databasing. Also with animiation of the object was working but won't be put into use until after sprint 7.
</p>

## 3. What have I learned?
<p>
I've learned more about how components and hooks work within the ionic/react enviroment and/or codebase.I've learned alot more about html tags in relation to webcam display and the like. 
</p>

## 4. What still puzzles me?
<p>
I'm still not sure how to combined the use of the webcam and the AR canvas from react-three-fiber/xr (WebXR), however, Aiden borught up a good idea that we can maybe implement for our next sprint.
</p>

## 5. What will we change to improve? 
<p>
We can still continue to meet outside of class. Other than this, there isn't much else I can say for the entire gourp but something I can personally improve on is spending more time in one very specific area of the project as I feel as though I'm trying to be a jack-of-all-trades but I'm thinking it'd be better to really learn in depth 1 or 2 things within the project. Things such as rendering and bluetooth and how they can work together.
</p>

---
### Noah's Answers

## 1. What went well?
<p>
I have learned a lot about the backend side of our balloon storm app. I understand how we will be utilitzing mongoDB and Heroku to make a connection to an atlas data cluster.
</p>

## 2. What didn't go so well?
<p>
I've yet to establish a proper connection between the atlas data cluster and our app.
</p>

## 3. What have I learned?
<p>
Learned about MongoDB atlas and heroku. How to query an atlas cluster, write/read, etc.
</p>

## 4. What still puzzles me?
<p>
How to make the connection by using a server.js file and calling the script.
</p>

## 5. What will we change to improve? 
<p>
I will be looking more into how node.js and possibly express works to hopefully better understand how to make our backend and make a connection between database and our app.
</p>

---
### Duncan's Answers

## 1. What went well?
<p>
I can now pass information from the accelerometer and button to where it needs to be.  I can also reset the accelerometer to zero.  I also performed some massive reorganization and cleanup on the existing BLE codebase.  This refactoring should make the BLE components easier to work with later on.
</p>

## 2. What didn't go so well?
<p>
Refactoring/reorganizing broke the button color change functionality on the diagnostics page.  This seems to be because the useState hook used to mediate the changes causes the page/component to infinitely re-render.  This is a problem.
</p>

## 3. What have I learned?
<p>
I learned a lot about how props work and how interfaces work.  Also, splitting up the components into discrete files makes it easier to navigate and edit the code that I've been working on.  Refactoring/renaming the different files and components that I use should make it easier to tell the intent behind the stuff that I write, which will be useful going forward, since it's getting to the point where others are going to have to work more directly with my code.
</p>

## 4. What still puzzles me?
<p>
Other types of react hooks.  I've learned a lot about useState, but I'll need to learn more about useEffect hooks going forward. I'm not certain that I'll be able to use useState for object manipulations.
</p>

## 5. What will we change to improve? 
<p>
Things seem to be going pretty smoothly -- group-wise I don't see much need for improvement.  Our group meetings seem to be adequate to discuss what's going on.  Personally, I need to improve my documentation somewhat.  A largish portion of this sprint was spent reorganizing my parts of the code to make it easier to work with.  By taking some time to plan things out before jumping straight into coding, I might be able to head some of these issues off.
</p>

---
### Aiden's Answers

## 1. What went well?
<p>
We were able to get more work done on bluetooth connectivity and reading data off of the wand. We were also able to make good progress on the AR space, but had a merge conflict with the object rotation code.
</p>

## 2. What didn't go so well?
<p>
A failed merge near the end of the sprint haulted a lot of progress in the object rotation code. The code for that still works, but there are issues with dependencies and whatnot that are just being a pain. Working on getting it sorted out though.
</p>

## 3. What have I learned?
<p>
I've learned a lot about webXR and drei, and how they work inside a react-three-fiber program. I'm also learning git more flently, I feel even more confident in using it.
</p>

## 4. What still puzzles me?
<p>
I'm still puzzled with a lot of dependency work. I've been running into an issue late this week with one of my packages installed not being compatable with the project, and React was yelling at me big time for it. I ended up cloning a new repository on my computer and being fine, but it was stressful getting through it.
</p>

## 5. What will we change to improve? 
<p>
I don't feel like a lot of new challenges came about this week that we need to work on as a group. But, I think that if Duncan, Noah, and I work together, we can make a lot of progress in getting the wand to do cool things on the screen. 
</p>

---

### Joey's Answers

## 1. What went well?
<p>
Having the responsibility of setting up the database connection split between two people helped me focus on working on a single potential solution. Previously, I was trying to get the solution using Heroku for the server connection working while also considering an overwhelming amount of different possible solution. Splitting this portion with Noah to have him focus on Heroku helped me focus on one thing as well.
</p>

## 2. What didn't go so well?
<p>
Having an essay, two discussion posts, and two programming assignments due for other classes over a span of four days robbed me of a lot of time this week.
</p>

## 3. What have I learned?
<p>
I was able to focus on researching how MongoDB realm is supposed to work together with MongoDB Atlas and I think I can make the connection to the database work without the Heroku repository.
</p>

## 4. What still puzzles me?
<p>
The specifics of how to implement what I've learned about MongoDB, but I believe these questions will be answered as I attempt to implement these things.
</p>

## 5. What will we change to improve? 
<p>
I don't think anything from the last week needs to change. I learned a lot from my research and am ready to continue on that path to apply what I've learned.
</p>
