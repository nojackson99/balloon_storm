### Ryan's Answers

## 1. What went well?
<p>
We were able to get an object to render using React Three Fiber which was the main goal and we also added more functionality to our application with our kano wand.
</p>

## 2. What didn't go so well?
<p>
We haven't yet gotten the database mongoDB integratee quite yet but our main mission is to get that integrated within the next sprint. We also weren't yet able to get object rotation on our object but we are close to doing so.
</p>

## 3. What have I learned?
<p>
I've learned more about our React Three Fiber library that we'll be using in our applciation which is vital to the augemnted reality application.
</p>

## 4. What still puzzles me?
<p>
We still need to get the camera integration going with the AR space. I do have an idea of what to do with this so it's not totally lost on me yet.
</p>

## 5. What will we change to improve? 
<p>
I think we did decent in that we hit some goals for this sprint 7, however, in order for us to get the object rendered, it took 3 team members to get it working, and although it's not ideal to have 3 people on one central issue, we did get the job done. We've delegated one of our augetned reality team members to move to databasing to assist in that regard, which I think will be more of a balanced team effort. We only have one team member on databsing which wasn't, in my opinion, fair to them so moving forward, we'll make sure we help cover each other and assit when needed. This past sprint 6 we did meet slightly more often, we intend to keep that going moving forawrd and meeting at least once more outside class.
</p>

---
### Noah's Answers

## 1. What went well?
<p>
We figured out that we would need to use React three fiber in order to utilize three.js functions in our project. Before we though that three.js by itself could be usied in an ionic react project. Using react three fiber we were able to get something rendered onto a page and this render will eventaully evolve into our full ar space for the game
</p>

## 2. What didn't go so well?
<p>
Progress is still very slow as it took 3 team members over a week to figure out what technology we would need to use to successfully render an object. There is still a lot of work to do in order to turn the basic render of one cube into an enitre ar game.
</p>

## 3. What have I learned?
<p>
I learned how to render an object using react three fiber onto a react project.
</p>

## 4. What still puzzles me?
<p>
How to integrate the camera and use it as the background and how exactly objects will be rednered to best represent the game.
</p>

## 5. What will we change to improve? 
<p>
Not too many changes will be made, Ryan and Aiden will continue to work on object rendering and I will switch over to helping Joe with connecting our mongodb database to our project code.
</p>

---
### Duncan's Answers

## 1. What went well?
<p>
Keep alive functionality is working, to the best of my ability to test.  Ditto the basic bits for the button timer.  Everything went together pretty straightforwardly, so that's good.
</p>

## 2. What didn't go so well?
<p>
Still trying to work out props and how to pass information from the tilt sensor quaternions to other components.
</p>

## 3. What have I learned?
<p>
The wand has a separate characteristic used for keeping the connection alive.  It seems like it's just tapped every time a value is written to that characteristic.  Interestingly enough, it seems like a similar method is used to set the zero point on the motion sensor.  I guess I'll figure that out next week, though.
</p>

## 4. What still puzzles me?
<p>
The methods that I tried to use earlier in order to pass props within typescript didn't really work, so I need to find another way to pass values between components.  Since the other members of my group have cracked object rendering, this is going to take a higher priority in the coming weeks.
</p>

## 5. What will we change to improve? 
<p>
There doesn't seem to be much to improve on this time around.  Everyone seemed to have their stuff pretty well worked out and were able to get ahold of 
</p>

---
### Aiden's Answers

## 1. What went well?
<p>
Ryan and I were able to implemenmt React-Three-Fiber (RTF) into the code so that an object would get rendered on our webpage. This was a huge breakthrough that had been holding us up for weeks, and now we can start implementing object functionality with the wand.
</p>

## 2. What didn't go so well?
<p>
We were not able to fo much within RTF, since we only made the discovery this past weekend. Only a purple square shows up when the start button is pressed, but that will change quickly.
</p>

## 3. What have I learned?
<p>
Ryan and I have begun looking into RTF. Even though there is not too much documentation on RTF, a lot of the core principals from threejs are carried over into RTF (which is essentially just threejs with React componenets)
</p>

## 4. What still puzzles me?
<p>
RTF is going to be using a lot of hooks for its core animation functionalities, and I still have very limited knowledge on how hooks work. Hoping that will change this sprint.
</p>

## 5. What will we change to improve? 
<p>
It's hard to pinpoint a specific change our group needs to make to improve, but I feel like we could do better about putting in commits regularly.
</p>

---

### Joey's Answers

## 1. What went well?
<p>
Although a different method of styling was merged to main, I was able to learn and implement styled ionic components in a different branch.
</p>

## 2. What didn't go so well?
<p>
I could not find relevant information online about using Heroku with the Ionic framework and mongoDB. Furthermore, there is an abundance of information on various methods to create a backend API using node.js. I have not been able to find one that I think I understand enough to convert and make compatible with the Ionic framework.
</p>

## 3. What have I learned?
<p>
How the Ionic framework has it's own system for styling in styled components rather than css files. This is something I knew from very early on in development, but I did not understand how to implement this system.
</p>

## 4. What still puzzles me?
<p>
I don't know if I should continue to persue Heroku as the backend component to connect our app to the mongoDB Atlas database, or if Express/Axios is a better method. There is a lot of documentation on using Express with react and node.js, but I don't know how much the Ionic framework might interfere with this.
</p>

## 5. What will we change to improve? 
<p>
We are redistributing work load as far as who will be working on what parts of the project.
</p>
