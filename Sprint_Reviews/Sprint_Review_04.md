### Ryan’s Answers

## 1. What went well?
<p>
We hadn't encountered any merge conlficts that puzzled us. We were abel to work out very small conflicts since we were communicating with eachother to help avoid them when we would merge. Most of our code is able to work with eachother. When connecting through bluetooth, we're able to stay conencted throughout the page after using some menu selections and functions.
</p>
## 2. What didn't go so well?
<p>
It didn't seem like, as a whole, our group committed enough which did make finding bugs tougher.
</p>
## 3. What have I learned?
<p>
I've learned more about the bluetooth wand and the bluetooth wand components and how the connectivity works. I have more to learn about it, but the laerning process is going alright so far.
</p>
## 4. What still puzzles me?
<p>
Trying to render our 3D object onto the screen without it interfering with our homepage is becoming challening. What's also puzzling me is how we can render the 3D objects without our "hiro" markers. We're coming closer to a solution, however, which is encouraging.
</p>
## 5. What will we change to improve? 
<p>
We'll work on committing more throughout the week in oder to follow our work more closely. Doing so will help in working out bugs and complciations from newly merged code.
</p>

---

## Duncan's Answers

### What went well?
I can pull useful data from the wand to make changes on a page.  The parser functions for the wand work well, and changes on notification.  I can also connect to the wand regularly.

### What didn't go so well?
I'm still working on pulling useful data from the accelerometer.  Right now, I can output raw data to the console, but it's still a bit of a far cry from being able to do anything useful with that data.

### What have I learned?
Commit early and often when working on your own branch.  Don't git reset --hard blind.  Also, css for ion elements is a little bit wonky and requires special handling.  Also, the wand doesn't broadcast its services, which makes it difficult to filter for it in a congested bluetooth environment.

### What still puzzles me?
Callback functions.  They seem to work a little differently in typescript than in javascript.  I'm going to need to crack that before I can make use of the accelerometer data.

### What will we change to improve? 
Aside from having to do more react research, most of our work is going pretty smoothly.  I'm not sure how to improve on what we're doing.

---

### Aiden's Answers

## 1. What went well?
<p>
Progress is being made on getting objects to render. GLTF files are tricky since they're such a new file format, but there are libraries
I'm experimenting with to interact with the camera
</p>
## 2. What didn't go so well?
<p>
I keep hitting a lot of dead ends when it comes to rendering objects inside ionic/react. I'm having trouble trying to load JS code in our combinedMenu page.
</p>
## 3. What have I learned?
<p>
I learned about Ionic/React components and routing this week. Earlier on this week I was having issues trying to get my page routed to from the main page, then we all decided we want to try and get our code to run from one central file.
</p>
## 4. What still puzzles me?
<p>
I'm still trying to figure out Ionic/React compatibilities with certain JS libraries that I'm finding. Some of them, like ar-js, do not seem to want to work with I/R.
</p>
## 5. What will we change to improve? 
<p>
There aren't many workflow changes I feel that need working on. As a group we're making great progress, but rendering has proven to be a bigger challenge than I had initially anticipated.
</p>

## Joey's Answers
### 1. What went well?
I was able to create a mock-leaderboard and set up a database on mongoDB Realm and mongoDB Atlas. Buttons now change what is rendered on the combinedMenu page.
### 2. What didn't go so well?
The is only documentation on mongoDB's website for Ionic Native and nothing from Ionic React.
### 3. What have I learned?
According to team members. Ionic React can utilize pieces of Ionic Native.
### 4. What still puzzles me?
How to order queries for a document database.
### 5. What will we change to improve?
Try to use more solutions for Ionic Native instead of assuming Ionic React is too different to utilize.

---

### Noah's Answers

## 1. What went well?
<p>
Our team progressed on getting an object to render with our chosent library which in this case is three.js.
</p>
## 2. What didn't go so well?
<p>
Stil unsure if three.js will work for the gameplay and rendering of all balloons.
</p>
## 3. What have I learned?
<p>
I have learned more about gltf files and how they can be rendered though various methods such as aframe, ar.js, or three.js
</p>
## 4. What still puzzles me?
<p>
I am still unsure of how typescript can import and use javascript and html code.
</p>
## 5. What will we change to improve? 
<p>
Continuing to do more research and keep our focus on our various parts of the project.
</p>

---
