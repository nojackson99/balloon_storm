### Ryan's Answers

## 1. What went well?
<p>
Not much in terms of progression. I've learned a little, though.
</p>

## 2. What didn't go so well?
<p>
For me personally, I'm not progressing with the WebXR on the android phone and trying to get the camera activated on our application.
</p>

## 3. What have I learned?
<p>
I've learned more about WebXR which is an offshoot of react three fiber. 
</p>

## 4. What still puzzles me?
<p>
I still cannot figure out how to enable WebXR to be "available" for when I launch the augmented reality xr tag, for exmaple, < ARCanvas > but it's not working. I beleive it has something either to do with the permissions on teh device and/or I need to use the tips used by following this link: immersiveweb.dev which gives possible reasons to why it may not working. 
</p>

## 5. What will we change to improve? 
<p>
Meet more often and spend more time on the sprint. I spent an average amount this week on my code, but I could've spent more time with it, personally.
</p>

---
### Noah's Answers

## 1. What went well?
<p>
I was able to get balloon storm deployeed onto android using my one plus 7 pro, which is good. I also found an ionic plugin that I can use to implement android permissions.
</p>

## 2. What didn't go so well?
<p>
So far attempting to get balloon storm to properly ask for permisions is not working. But I haven't spent a ton of time working on this part so before the end of the next sprint I fell that premissions will be implemented.
</p>

## 3. What have I learned?
<p>
I have learned more about android development and about how permissions are implemented on android through the android manifest file and through functions in our code
</p>

## 4. What still puzzles me?
<p>
Not sure if all that is needed for connecting the wand to our app on android is the permissiosn or if other code needs to be written to facilitate this connection.
</p>

## 5. What will we change to improve? 
<p>
I will continue to work on my part of the project and work with the rest of the team to ensure that we have something solid to show for our final product.
</p>

---
### Duncan's Answers

## 1. What went well?
<p>
Not a whole lot.  Implementing new components for the wand reticle and the basic balloon was relatively straightforward.  That's about all the success that I've managed to get this week.
</p>

## 2. What didn't go so well?
<p>
Getting smooth animation is problematic, it seems to be tied to the screen refresh, which works perfectly fine when it's only one element, but the more elements I add, the more it seems to bug out.
</p>

## 3. What have I learned?
<p>
Tying the animation to the screen refresh is a non-starter, since any change will likely result in a screen refresh.  This is bad.
</p>

## 4. What still puzzles me?
<p>
Animation needs to not take place every refresh.  Minimally, I need to find a way to make an independent 'clock' that I can base the animations upon, rather than relying on the wand's refresh rate.
</p>

## 5. What will we change to improve? 
<p>
I'll work more with Aiden on the animation side of things.  Between the two of us, we should make some progress.
</p>

---
### Aiden's Answers

## 1. What went well?
<p>

</p>

## 2. What didn't go so well?
<p>

</p>

## 3. What have I learned?
<p>

</p>

## 4. What still puzzles me?
<p>

</p>

## 5. What will we change to improve? 
<p>

</p>

---

### Joey's Answers

## 1. What went well?
<p>
Educating myself on GraphQL and how react uses gql and picks apart the information returned to it.
</p>

## 2. What didn't go so well?
<p>
I have been working off a tutorial that involves Ionic React and gql to access MongoDB. I excluded a piece related to signing in to the tutorial app because our app doesn't involve user accounts. However, the tutorial used this sign-in to GraphQL. Excluding this ended up creating errors that will have to be resolved next sprint.
</p>

## 3. What have I learned?
<p>
The layout of gql queries and how the information returned mimics this layout.
That gql requires a sign-in to use.
</p>

## 4. What still puzzles me?
<p>
I haven't been able to find anything on selecting data from gql and ordering them by a certain value. Because of this, I am still wondering how I will end up selecting the top 5 scores to display on the leaderboard page.
</p>

## 5. What will we change to improve? 
<p>
We will continue to try to meet more outside of class since we have not done so in the past week or two. I should also be able to spend more time on the project, now that major assignments in other classes are coming to a close.
</p>
