### Ryan's Answers

## 1. What went well?
<p>
A lot of ouu user stories in the last sprint were finished and completed. We've gotten hitboxes on the objects, we've changed the shape fo teh reticle and the balloon object so that they're accurate to their portrayal. We have the reticle changing colors according to the color of the balloon object and you can only pop the balloon on the color of the reticle, making the game more interactive. We ahve a point system on which colored balloon you pop and those live balloon popping gives feedback to your points that show live on screen as you play. We have a login/registration page working with mongoDB Realm as instructed in the objefctives of the project description. We've gotten the bluetooth working on the Adnroid version as well. Lastly, we have a timer going that when you rpess the play button, it starts the timer.
</p>

## 2. What didn't go so well?
<p>
We're having trouble getting the game to reset when you rpess the "Quit" button but we haven't been able to figure that ou quite yet. We also are having trouble getting past the login/registration page on Android, but we have a workaround to that in case we can't figure out the permissions on the Andorid in time.
</p>

## 3. What have I learned?
<p>
I;ve learned qutie a lot more about Typescript, Javascript, bluetooth low-energy, team management and working together to acheive a goal.
</p>

## 4. What still puzzles me?
<p>
I still can't figure out how to reset the game when pressing the "Quit" Button and then pressing the "Start" button. The "Start" button restarts the timer but not the game itself.
</p>

## 5. What will we change to improve? 
<p>
We had a very strong sprint 12, we can continue to meet outside of class which is what I did quite a lot this week. I met up with Noah outside of class and metup with Duncan several times over Discord to dicsuss the issues with the application and how to remedy those problems. 
</p>

---
### Noah's Answers

## 1. What went well?
<p>
We have the wand now properly connecting to android version of Balloon sotrm. Also a lot of work has gone into making the actual game part work, and it's now in a good place for the demonstration.
</p>

## 2. What didn't go so well?
<p>
I wasn't able to work on splitting our game canvas view so that it works with the headset, still looks pretty wonky on there but due to time constraints we will even our development here and focus on the presentation and documentation.
</p>

## 3. What have I learned?
<p>
I have learned that when I have an issue I'm tyring to solve and I'm not making any progress I should rethink about my problem and ask myself if the fixes I am tyring to implement will fix the problem or if I should come at it from a new direction
</p>

## 4. What still puzzles me?
<p>
I think there are a lot of advanced react topics that I don't fully understand after working on this project.
</p>

## 5. What will we change to improve? 
<p>
Nothing much, we're at the end of the deveopment so not much else to add. Now our main focus will be on preparing for the presentation and addressing any other issues that will affect our presentation.
</p>

### Duncan's Answers

## 1. What went well?
I accomplished all my major goals.  The wand reticle changes color with a wand motion, the balloons will spawn in a random position with a random color, and there is a random amount of delay from the time the balloon despawns to when a new balloon spawns.  I'm actually pretty excited about this particular update.

## 2. What didn't go so well?
The solutions that I came up with to accomplish these goals aren't quite as refined as I would have liked, though at this stage in the game, there's not likely anything to be done about that.  Also, there's a minor issue with BLE permissions on android that cropped up, which is a minor embarassment, if I'm being honest.

## 3. What have I learned?
I learned that you can do modulo operations on a float, which in retrospect isn't terribly surprising, but it threw me for a loop for a little bit.  Also, while typescript does technically have an integer type, it's not the default.  Math.floor() was my friend in this situation.  I also learned to try not to assume that something won't work, and to just try something even if it seems stupid.  After trying for a week to bring in values to the button parser from the accelerometer notification function, I was advised to try just making a cache variable.  Apparently that worked as intended.  Everything I'd learned about the way react works suggested that there wasn't that kind of continuity between reloads.  I guess I have more to learn.

## 4. What still puzzles me?
There are some minor issues that need fixed, but nothing that is particularly 'puzzling'.  There's some stuff that I've got questions about, like the react framework, but it's not particularly relevant to this project anymore.  Likewise, much of the database code is a black box to me since I haven't really had a reason to look into it.  Does that count as puzzling?

## 5. What will we change to improve? 
This is the last sprint, so there's not much to improve on with regards to this project.  Since I am hopeful that we'll freeze any further feature development after today, we shouldn't have too much need to meet up about the program.

---
### Aiden's Answers

## 1. What went well?
<p>
I made pretty good progress with the scoring system this week. I was able to make a class called Score and get a Score generated when the play button is hit, and then was updated on an instance of a balloon being popped. But, while Ryan was crunching out the object rendering, he implemented a solid scoring system on his local repo. So, the group just decided to go with that. I still have some pretty good code for that, but sadly it won't be used.
</p>

## 2. What didn't go so well?
<p>
This week I had trouble getting the score to update whenever the hitbox was triggered. That was my main obstancle I tried to overcome this week, but Ryan's score system isn't dependent on hitboxes. I suppose if we had more time, a better score system could have been implemented. But, this scoring system works!
</p>

## 3. What have I learned?
<p>
I've learned a lot about class structure in the React framework, and how interfaces are used to typecheck props coming into certain components. My local repo didn't have all of the different implementations that Ryan and Duncan's did, but I learned to be able to work with what I have.
</p>

## 4. What still puzzles me?
<p>
I'm still puzzled how I would've gotten the score to update whenever a hitbox is triggered. I'm positive it would be done through a boolean, but if it was a boolean, we wouldn't have been able to implement different score amounts to be given.
</p>

## 5. What will we change to improve? 
<p>
Since this is the last sprint, I'm not sure there's much that can be improved on. I've improved leaps and bounds from where I was at the start of this semester, and I'm glad I've picked up knowledge of these tools, regardless of whatever grade I end with in this class.
</p>

---

### Joey's Answers

## 1. What went well?
<p>

</p>

## 2. What didn't go so well?
<p>

</p>

## 3. What have I learned?
<p>

</p>

## 4. What still puzzles me?
<p>

</p>

## 5. What will we change to improve? 
<p>

</p>
