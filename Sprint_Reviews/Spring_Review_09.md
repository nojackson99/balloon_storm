### Ryan's Answers

## 1. What went well?
<p>
My Androidn Development has gone well so far in getting our application ported into Android Studio and then running an emulator. The emulated device is a Nexus 6P device on mine (Ryan's) computer. We're still working on getting Adnroid Studio working on others' computers.
</p>

## 2. What didn't go so well?
<p>
There were soem errors that I encountered with Adnroid Studio but I eventually figured them out. We also had some difficulties with merging some branches but Joe and I figured it out just fine and remidied the conflicts. 
</p>

## 3. What have I learned?
<p>
I've studied up and learned more in-depth of the bluetooth code that Duncan wrote in sprint #8. I've learned a decent bit about Android Studio and how to make it work, using capacitor, to run our applciation on an emulated Android device.  
</p>

## 4. What still puzzles me?
<p>
I'm still not sure how to approach getting Android Studio to run on my physical Nexus 6P mobile phone. I haven't quite tried this since my mission was to get the emulation going, but I plan on attempting this in sprint #10. I know Joe is making good progress with ou database and its working, we just need to figure out (in code) to send off pieces of data to Realm (the middleman) then to Atlas.
</p>

## 5. What will we change to improve? 
<p>
We can continue to meet more often. Since there's alot going on with our Android Development and databasing along with object animation, communication is more key now than ever.
</p>

---
### Noah's Answers

## 1. What went well?
<p>
Started working on deploying balloon storm on android and we have successfully have the app running on an android emulator. We also have progress in getting the database to connect to balloon storm using ionic realm instead of using heroku
</p>

## 2. What didn't go so well?
<p>
Wasn't able to get the database working through heroku, I will be switching away from database work over to working on deploying our app on android.
</p>

## 3. What have I learned?
<p>
I learned some about the android development process.
</p>

## 4. What still puzzles me?
<p>
I'm still working on getting our app to work on an actual android phone. I've only spent one day working on this so over the course of the week I will learn more about how porting ionic apps to android works.
</p>

## 5. What will we change to improve? 
<p>
Not much to imporve, we seem to be making progress on all fronts.
</p>

---
### Duncan's Answers

## 1. What went well?
Implementing the game canvas and the basics for the reticle were pretty straightforward.  Modularizing the different components at the outset was super helpful and has sped some of the development up considerably. 

## 2. What didn't go so well?
Figuring out the animation is a bit of an uphill slog.  I would prefer to avoid excessive callbacks, but I'm not 100% sure how to do that.  Since the wand motion updates are going to cause the page to refresh anyway, I might be able to tie the animations to that refresh.  Hopefully.

## 3. What have I learned?
Much of the work that I have implemented in the BLE_Diagnostics could be more or less directly ported over to some of the other portions of code.  I had intended this from the outset, but am kind of excited to see it pan out so well in practice.

## 4. What still puzzles me?
Animating the objects.  Also, where is the camera relative to the canvas?  The animation rules are going to be relatively simple, but I'm concerned about how well the techniques I'm using for movement will translate to multiple objects.

## 5. What will we change to improve? 
More meetings.  I'm probably going to have to meet more with the rendering team to figure out how to fit everything together animation-wise.  I should also do some research on my own to find out if there are some in-built animation solutions.

---
### Aiden's Answers

## 1. What went well?
<p>
I'm understanding the gltfjsx library a little bit better, with exporting the gltf code as React native code. Still am not successful in getting the gltf to load, so it may be time to look for a different way of implementing rendered objects in the playsphere. Keeping my options open though
</p>

## 2. What didn't go so well?
<p>
No tangible progress was made with rendering gltfs. I'm almost spinning my gears in place trying to get this to load onto the screen, but I can't seem to figure out how to get that stupid hook to work. I've been on it for like 2 weeks and I still can't get this bug fixed...
</p>

## 3. What have I learned?
<p>
Above all else, patience. Loading to gltfs into the scene is way harder than I thought it would be, even though these tutorials make it look so easy. I'm determined to get something to load up, I've hit a pretty large standstill.
</p>

## 4. What still puzzles me?
<p>
Do I need to have the hook in a certain place? Am I missing a certain dependency? Should I create a separate subcomponent and load it into the object rendering page, or can I do it from the page itself? These are all questions I'm having trouble answering.
</p>

## 5. What will we change to improve? 
<p>
I'll probably need to reach out to Ryan or Duncan for help on this. Both of them understand hooks way better than I do, there's still some concept flying over my head and I don't even know what concept it is. Overall, a not very productive week on my end:/
</p>

---

### Joey's Answers

## 1. What went well?
<p>
I added functions for context that will hold the API for MongoDB Realm for our application. I added a provider that will populate said context function. These are implemented on the application level via wrapper tags in App.tsx. These changes have not yet been merged to main, since I still need to create hooks that will implement CRUD operations for our application to interact with the MongoDB Atlas database.
</p>

## 2. What didn't go so well?
<p>
I thought that I would be able to use this week to implement CRUD operations but, since I learned about the need for context and providers I used up all of my time this week researching how to implement those things before I could work on the CRUD ops.
</p>

## 3. What have I learned?
<p>
Initializing our app with realm-cli to push our code onto MongoDB Realm did not give our app the ability to "talk" to the Realm, it just gave the Realm the ability to "talk" to our app. The wrapper tag with the Realm app ID passed into it should provide this functionality.
</p>

## 4. What still puzzles me?
<p>
The tutorial I am following to learn more about using MongoDB Realm with Ionic React does not have any implementation for selecting and displaying information from the database into the app, so I am curious to see if I will be able to use the resources for using Realm with node.js to put together a solution.
</p>

## 5. What will we change to improve? 
<p>
Personally, this past week I have spent less time trying to research for a solution that I think will work, and more time trying to implement anything that I am able to find through research. This has helped me because I no longer second guess if something I am reading about will work, and trying to implement something that doesn't work is not as detrimental as I thought it would be. 
</p>
