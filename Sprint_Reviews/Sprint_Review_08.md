### Ryan's Answers

## 1. What went well?
<p>
For me personally, not much. I'm still struggling with getting the camera integration into the applciation. We have been able to get bluetooth connectivity to our AR objects onscreen with our bluetooth kano wand.
</p>

## 2. What didn't go so well?
<p>
We're stills truggling on making progress on several categories of the project, such as databasing and camera funcitonality.
</p>

## 3. What have I learned?
<p>
I've just learned more about React and react-three-fiber and its corresponding libraries such as react-three-fiber/xr and react-three-fiber/drei.
</p>

## 4. What still puzzles me?
<p>
I still don't know how to approach our camera integration into the application and how to combine it with our AR Canvas playspace. There is some hope in that I'll need to begin the Android (and ios) development which can render augmented reality with our react-three-fiber/xr library, however iOS has trouble with it and I'm looking further to trying to remedy this.
</p>

## 5. What will we change to improve? 
<p>
We need to meet more often outside of class and all spend more time in our code. We're stuggling to continue making progress on several key parts of the project. We do have progress with our bluetooth integration with the augmented reality objects. We are behind schedule but am keen on making progress during sprints 9 and 10.
</p>

---
### Noah's Answers

## 1. What went well?
<p>
Making some progress on finally making a database connection. I have a function worked out that will do the connection and document insertions but I'm not sure if my function will work and is written in the right way. I've also done some coding with how we will take the name and score data format that into a document and pass to the insert function.
</p>

## 2. What didn't go so well?
<p>
Progress is not moving as quickly as I would like it to, I am hoping that in the next week or so I have successfully made the connection and can insert data into the database.
</p>

## 3. What have I learned?
<p>
Not to much has been learned this week
</p>

## 4. What still puzzles me?
<p>
I am still unsure of how to deploy my database connection function in main project. How to import the function into our combined menu page.
</p>

## 5. What will we change to improve? 
<p>
I will continue working over the next week on the database, and I hope to make a breakthrough.
</p>

---
### Duncan's Answers

## 1. What went well?
Wand movements can now move an object on the screen, pressing the wand buttons will change the color of the light shining on the cube, essentially changing its color.  Most canvas supporting functions have been shifted over to another page, which makes it easier to navigate what I'm doing.  Also, the keepAlive function has been altered so it's not being activated with every button press, which should free up some bandwidth.

## 2. What didn't go so well?
Normalizing the wand movements to the screen is proving difficult.  I had assumed that the three.js camera was more directly related to the canvas resolution.  That does not appear to be the case.  I'll have to do some more math to normalize.  Also, there's not really a very good way to maneuver in the Z-axis, which might cause issues with targeting.

## 3. What have I learned?
The three.js canvas uses a coordinate system that does not exactly correlate to the canvas resolution.  This might be due to the use of a "camera" to emulate depth-of-field.  This camera also produces some distortion around the edges, depending on the aspect ratio of the canvas.  With extremely wide fields of view, these distortions are more pronounced.  I'm not 100% sure what this is going to mean going forward.


## 4. What still puzzles me?
Calculating position in a 3-d space, and what the coordinates in the three.js canvas actually mean.  These are both issues that are going to have to be solved before we can get to work 'popping' balloons.


## 5. What will we change to improve? 
Personally, I'm happy with the quality and quantity of the work that I'm producing, so I'm not sure that I see much reason to change there.  I'll need to look more deeply into some of the three.js and react-three fiber documentation so that I have a better idea of how they work, since my work is beginning to intersect more with that.  Everyone else seems to be progressing adequately as far as I'm concerned, though they are perhaps too concerned with having a polished product.  In my estimation, that was never the intent of this project.

---
### Aiden's Answers

## 1. What went well?
<p>
We were able to get the objects loaded on the screen to sync with the want! Big progress there.
</p>

## 2. What didn't go so well?
<p>
Nothing too much went on with my side of things. I've been trying to implement hooks into the rendering section, but I can feel like there's a single concept I'm missing that's gating me. Once I fully understand hooks, I know they'll be powerful for rendering.
</p>

## 3. What have I learned?
<p>
I've learned the structure of hooks and how they're written, and I've learned of the capabilities of certain hooks in certain libraries.
</p>

## 4. What still puzzles me?
<p>
I don't fully understand what the code I write involving hooks actually does behind the scene. There are certain instances where I try to use hooks, but I'd get an error involving IntrisicAttributes.
</p>

## 5. What will we change to improve? 
<p>
I've been trying to find as much free information as I can online about what I'm doing, but it might be time to put forth 15$ for an online course better explaining everything. End of the semester is quickly approacing, so it's crunchtime.
</p>

---

### Joey's Answers

## 1. What went well?
<p>
I was able to research how to log in to realm via the terminal and the realm-cli commands in order to push our code to the MongoDB Realm.
</p>

## 2. What didn't go so well?
<p>
Writing the BSON schema that the Realm will use to validate objects being submitted to the highScores collection in the BalloonStormData Atlas database.
</p>

## 3. What have I learned?
<p>
The MongoDB system uses GraphQL to reference objects in its databases. These databases and their collections are formatted uses BSON schema.
</p>

## 4. What still puzzles me?
<p>
How to write a BSON schema that is accepted by MongoDB Realm.
</p>

## 5. What will we change to improve? 
<p>
I don't know what can be changed to improve the development of the backend database. It is like it was said at the start of the semester; this class shouldn't have been taken along with a full schedule, and I am enrolled in two other 4000 level classes and two 3000 level classes. I am trying to contribute as much time as I can to this project while also trying to pass the credits I need to graduate this semester, and it has not been easy.
</p>
